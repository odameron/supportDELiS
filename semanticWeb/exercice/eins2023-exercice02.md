---
title: "Exercice 2 : read information from an RDF graph in turtle syntax"
tags: eins2023, teaching, SemanticWeb, RDF
version: 1.0
date: 2023-07-04
---


Consider the following RDF graph:

```turtle
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX dct: <http://purl.org/dc/terms/> 
PREFIX ex: <http://example.org/tutorial/>

ex:p1 rdf:type ex:Protein .
ex:p1 rdfs:label "Protein 1" .

ex:p2 rdf:type ex:Protein .
ex:p2 rdfs:label "Protein 2" .

ex:p1 ex:hasMeasure ex:m1 .
ex:m1 rdf:type ex:Measurement .
ex:m1 ex:value 45.3 .
ex:m1 ex:pValue 0.00078 .

ex:p2 ex:hasMeasure ex:m2 .
ex:m2 rdf:type ex:Measurement .
ex:m2 ex:value 3.1415926 .
ex:m2 ex:pValue 0.0001 .

ex:p1 ex:hasMeasure ex:m3 .
ex:m3 rdf:type ex:Measurement .
ex:m3 ex:value 42.1 .
ex:m3 ex:pValue 0.58 .
```

# 2.1 What are the classes?

```bash
# SOLUTION:
echo "MiBjbGFzc2VzICg9IG9iamVjdHMgb2YgdHJpcGxlcyB3aXRoIHRoZSByZGY6dHlwZSBwcm9wZXJ0eSk6Ci0gZXg6UHJvdGVpbgotIGV4Ok1lYXN1cmVtZW50Cg==" | base64 --decode
```

# 2.2 What are the instances?

```bash
# SOLUTION:
echo "NSBpbnN0YW5jZXM6Ci0gMiBpbnN0YW5jZXMgb2YgZXg6UHJvdGVpbiAoZXg6cDEgYW5kIGV4OnAyKQotIDMgaW5zdGFuY2VzIG9mIGV4Ok1lYXN1cmVtZW50IChleDptMSwgZXg6bTIgYW5kIGV4Om0zKQo=" | base64 --decode
```


# 2.3 Draw a visual representation of the graph

Solution: [exercice02.png](exercice02.png) (generated with `rapper --input turtle exercice02.ttl --output dot | dot -Tpng -o exercice02.png`)


# 2.4 Which protein(s) have a measure with a p-value < 0.05?

```bash
# SOLUTION:
echo "Qm90aCBleDpwMSBhbmQgZXg6cDIgaGF2ZSBhIG1lYXN1cmUgd2l0aCBhIHAtdmFsdWUgPCAwLjA1OgotIGV4OnAxIGhhcyBtZWFzdXJlIGV4Om0xIHdoaWNoIHAtdmFsdWUgaXMgMC4wMDA3OAotIGV4OnAyIGhhcyBtZWFzdXJlIGV4Om0yIHdoaWNoIHAtdmFsdWUgaXMgMC4wMDAxCk5vdGUgdGhhdCBleDpwMSBhbHNvIGhhcyBhbm90aGVyIG1lYXN1cmUgKGV4Om0zKSBidXQgaXRzIHAtdmFsdWUgaXMgZ3JlYXRlciB0aGFuIDAuMDUK" | base64 --decode
```

