---
title: "Exercice 3 : manually create an RDF graph in turtle syntax"
tags: eins2023, teaching, SemanticWeb, RDF
version: 1.0
date: 2023-07-04
---

Consider the following RDF graph:
![Simple RDF graph](../figures/rdf-graphInterpretation.png)


# 3.1 Write the RDF representation in turtle syntax of the graph

You can use the following template (use a basic text-editor such as notepad, and save in a file with the `.ttl` extension such as `exercice03solution.ttl`):

```turtle
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX dct: <http://purl.org/dc/terms/> 
PREFIX ex: <http://example.org/tutorial/>

# Your RDF triples here...
```

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBza29zOiA8aHR0cDovL3d3dy53My5vcmcvMjAwNC8wMi9za29zL2NvcmUjPiAKUFJFRklYIGRjdDogPGh0dHA6Ly9wdXJsLm9yZy9kYy90ZXJtcy8+IApQUkVGSVggZXg6IDxodHRwOi8vZXhhbXBsZS5vcmcvdHV0b3JpYWwvPgoKZXg6cDEgcmRmOnR5cGUgZXg6UHJvdGVpbiAuCmV4OnAxIHJkZnM6bGFiZWwgIlByb3RlaW4gMSIgLgoKZXg6cDIgcmRmOnR5cGUgZXg6UHJvdGVpbiAuCmV4OnAyIHJkZnM6bGFiZWwgIlByb3RlaW4gMiIgLgoKZXg6cDMgcmRmOnR5cGUgZXg6UHJvdGVpbiAuCmV4OnAzIHJkZnM6bGFiZWwgIlByb3RlaW4gMyIgLgoKZXg6cDEgZXg6YWN0aXZhdGVzIGV4OnAyIC4KZXg6cDIgZXg6YWN0aXZhdGVzIGV4OnAzIC4KZXg6cDMgZXg6aW5oaWJpdHMgZXg6cDEgLgo=" | base64 --decode
```


# 3.2 Check whether your file is syntactically valid

- (requires to have installed Apache Jena: retrieve `apache-jena-X.Y.Z.zip` from https://dlcdn.apache.org/jena/binaries/), and unzip somewhere. The extracted directory will be called `${JENA_HOME}`
- from a terminal, run `${JENA_HOME}/bin/riot --validate exercice03solution.ttl`
- if no error is reported, congratulation, proceed to the next question. Otherwise, read the error message, modify your file and iterate


# 3.3 (optional but useful) Generate a visual representation

(requires to have rapper installed; the additional layer from https://gitlab.com/odameron/rdf2image/ makes manipulation easier)
- low-level with rapper: `rapper --input turtle exercice03solution.ttl --output dot | dot -Tpng -o exercice03solution.png`
- higher level with rdf2image: `rdf2image -r exercice03solution.ttl -p exercice03solution.png`

![Solution for question 3](exercice03.png)
