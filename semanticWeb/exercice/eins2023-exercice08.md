---
title: "Exercice 8: federeated SPARQL queries"
version: 1.0
date: 2023-12-13
tags: [eins2023, teaching, SemanticWeb, RDF, ontologies, RDFS, OWL, SPARQL, federatedQueriesSPARQL, GeneOntology, UniProt]
---


# 8.0 Objective

This exercice demonstrates **SPARQL federated query** mechanism, i.e. a query that spans multiple SPARQL endpoints.

In principle, these can be any SPARQL endpoints.
Here, we will use the typical situation combining UniProt and GeneOntology. In this case:
- the UniProt public SPARQL endpoint is limited and does not accept references to other endpoints, so we will have to call UniProt from the GeneOntology endpoint;
- there is no public GeneOntology endpoint, so we will set a local endpoint.


# 8.1 SPARQL endpoints

## 8.1.1 UniProt SPARQL endpoint (remote)

The UniProt public SPARQL endpoint can be accessed at:
- for humans via a GUI interface: https://sparql.uniprot.org/
- for programs via the SPARQL protocol: https://sparql.uniprot.org/sparql


## 8.1.2 GeneOntology endpoint (local)

- retrieve GeneOntology from https://purl.obolibrary.org/obo/go.owl (`wget https://purl.obolibrary.org/obo/go.owl`)
- start fuseki (`${FUSEKI_HOME}/fuseki-server --file=go.owl /go`)


# 8.2 From UniProt: retrieve the GeneOntology terms associated to a protein


```sparql
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 

PREFIX upc:<http://purl.uniprot.org/core/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>

SELECT *
WHERE {
  VALUES ?protein { uniprotkb:P06213 }

  OPTIONAL { ?protein rdfs:label ?proteinLabel . } # rdfsllabel is present in the public UniProt endpoint but not in P06213.ttl
  ?protein upc:mnemonic ?proteinMnemonic .
  ?protein upc:reviewed ?proteinReviewed .
  ?protein upc:existence ?proteinExistence .

  ?protein upc:classifiedWith ?proteinAnnotation .
  FILTER(STRSTARTS(STR(?proteinAnnotation), "http://purl.obolibrary.org/obo/GO_"))
}
```

```sparql
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 

PREFIX upc:<http://purl.uniprot.org/core/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>

SELECT (COUNT(DISTINCT ?proteinAnnotation) AS ?nbAnnotations)
WHERE {
  VALUES ?protein { uniprotkb:P06213 }

  ?protein upc:classifiedWith ?proteinAnnotation .
  FILTER(STRSTARTS(STR(?proteinAnnotation), "http://purl.obolibrary.org/obo/GO_"))
}
```


# 8.3 From GeneOntology: retrieve the ancestors and labels of a GeneOntology term

```sparql
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 

PREFIX upc:<http://purl.uniprot.org/core/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>

SELECT DISTINCT ?goTerm ?goLabel
WHERE {
  VALUES ?goTerm { go:0005009 }
  ?goTerm rdfs:label ?goLabel .
}
```


```sparql
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 

PREFIX upc:<http://purl.uniprot.org/core/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>

SELECT DISTINCT ?goSuper ?goSuperLabel
WHERE {
  VALUES ?goTerm { go:0005009 }
  ?goTerm rdfs:label ?goLabel .

  ?goTerm rdfs:subClassOf* ?goSuper .
  ?goSuper rdfs:label ?goSuperLabel .
}
```


# 8.4 Combine the UniProt and GeneOntology queries into a single federated query

From your local GeneOntology endpoint

```sparql
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 

PREFIX upc:<http://purl.uniprot.org/core/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>

SELECT DISTINCT ?goSuper ?goSuperLabel
WHERE {
  # Retrieve the direct annotations from UniProt
  SERVICE <https://sparql.uniprot.org/sparql> {
    SELECT DISTINCT ?proteinAnnotation
    WHERE {
      VALUES ?protein { uniprotkb:P06213 }
    
      ?protein upc:classifiedWith ?proteinAnnotation .
      FILTER(STRSTARTS(STR(?proteinAnnotation), "http://purl.obolibrary.org/obo/GO_"))
    }
  }

  # Retrieve the annotations superclasses from GeneOntology
  ?proteinAnnotation rdfs:subClassOf* ?goSuper .
  ?goSuper rdfs:label ?goSuperLabel .
}
```


