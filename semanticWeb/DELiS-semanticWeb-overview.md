---
title: "Data Engineering in Life Sciences : Semantic Web"
tags: teaching, semanticWeb, RDF, SPARQL, OWL, ontologies
version: 1.0
date: 2023-11-29
---

# Semantic Web principles

The core material is available at: https://gitlab.com/odameron/eins2023


# Installation instruction

Instructions for installing fuseki are available at: https://gitlab.com/odameron/fusekiInstallationUsage


# SPARQL

- Bob DuCharme: SPARQL in 11 minutes https://www.youtube.com/watch?v=FvGndkpa4K0
- Cheatsheet: https://gitlab.com/odameron/rdf-sparql-cheatsheet

