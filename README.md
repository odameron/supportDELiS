Data Engineering in Life Sciences

This project contains the support for the Data Engineering in Life Sciences class.

**Outline:**

How to represent and query structured data?

1. Flat data in files, potentially connected: Tabulated data
2. Nested data in files, potentially connected: Tree-based data (XML+XSLT, JSON,...)
3. dataframes (R, python)
4. Relational algebra
5. Graph-based data
6. Knowledge graphs
	https://folderit.net/blog/graph-databases/
7. Semantic Web

> **Bibliography:**
> - From tabular data to knowledge graphs: A survey of semantic table interpretation tasks and methods. Jixiong Liu, Yoan Chabot, Raphaël Troncy, Viet-Phi Huynh, Thomas Labbé, Pierre Monnin. *Journal of Web Semantics*. 2023. [https://doi.org/10.1016/j.websem.2022.100761](https://doi.org/10.1016/j.websem.2022.100761)


# Tabulated files

![Dependencies between file manipulation commands](./tabulatedData/commandDependencies.png "Dependencies between file manipulation commands")


## Print a file

- print the whole file with `cat fileName`
- print the beginning (default the first 10 lines) of a file with `head fileName`
    - use `head -n 5 fileName` for the first 5 lines
    - use `head -n -5 fileName` for the first lines except the last 5
- print the end (default the last 10 lines) of a file with `tail fileName`
    - use `tail -n 3 fileName` for the last 3 lines
    - use `tail -n +3 fileName` for the last lines starting from the third line (included)
        - remove header: `tail -n +2 fileName`
- print in reverse order: use... `tac`



## Select the relevant columns with `cut`

- remove sections from each line of files
- default field delimiter is tabulation. Use `-d` or `--delimiter` for other characters
- extract one column
    - `cut -f 2 data/demo-sort.tsv`
- extract several columns
    - `cut -f 2,4 data/demo-sort.tsv`
    - NB: the order of the columns is always the one of the original file: `cut -f 2,4 data/demo-sort.tsv` VS `cut -f 4,2 data/demo-sort.tsv`
    - all the columns except: `cut --complement -f 2,4 data/demo-sort.tsv` VS `cut -f 2,4 data/demo-sort.tsv`



## Reorder lines with sort

- text in 1 column 
    - only lowercase, with duplicates
        - ascending
        - descending
    - mixed lowercase and uppercase
        - option -f?
    - only numeric values
        - notice that 5 should be < 23
        - option -n
- text with multiple columns
    - notice that by default
        - separator is the transition from non-blank to blank, so the second field may start with a tabulation (use `-b` to strip these blanks)
        - the `-k2` means "from field #2 to the end of the line" 
            - (use `-k2,2` to sort only on the second field)
            - notice that `-k2 -k3` is redundant and equivalent to the simpler `-k2`
        - source: https://unix.stackexchange.com/questions/52762/trying-to-sort-on-two-fields-second-then-first
    - compare
        - `cat data/demo-sort.tsv`
        - `sort -k4 data/demo-sort.tsv`
        - `sort -k4 -k2 data/demo-sort.tsv`
        - `sort -k4 -k2n data/demo-sort.tsv`
        - `sort -k4 -k2n -k3n data/demo-sort.tsv`
- headers?
    - use `head` and `tail`
    - `tail -n +2 demo.tsv`
	

## Randomly shuffle the lines with shuf (the anti-sort)



## uniq

- report or omit repeated lines (and count the number of repetitions)
- principles:
    - relies on comparing consecutive lines and counting how many times they are identical
    - repetition = 2 identical consecutive lines
    - `uniq` has a very limited short-term memory; it will never remember whether a line appeared earlier in the input, unless it was the immediately previous line -- this is why `uniq` is very often paired with `sort`
    - 3 modes
        - `default`: print **1** copy of each line but not of its potential following repetitions (print 1 iff >= 1 occurrences)
        - `--unique`: print **1** copy of each line (print 1 iff exactly 1 occurence)
        - `--repeated`: print **1** copy of only the lines that are repeated once or more (print 1 iff >= 2 occurrences)
- use cases:
    - what are the chromosomes that have at least 1 gene?
    - how many genes are there on each chromosome?
- `cut -f 4 data/demo-sort.tsv`
- repeated line = a line is the same as the previous one `cut -f 4 data/demo-sort.tsv | uniq`
    - sort before counting
        - `cut -f 4 data/demo-sort.tsv | sort | uniq`
        - `cut -f 4 data/demo-sort.tsv | sort | uniq --count`
        - NB: `sort | uniq` is equivalent to `sort -u`
    - focus on repetition or on uniqueness
        - `cut -f 4 data/demo-sort.tsv`
        - only the lines that **are** repeated `cut -f 4 data/demo-sort.tsv | uniq --repeated`
        - only the lines that are **not** repeated `cut -f 4 data/demo-sort.tsv | uniq --unique`



## grep


## comm


## join

**TODO:** show join

**TODO:** limits = explicitely compute successive joins


## Count the relevant lines

- exactly 1 category: `wc -l`
    - how many genes? 
        - `tail -n +2 data/demo-sort.tsv | cut -f 1 | sort` (warning: there are duplicates)
        - `tail -n +2 data/demo-sort.tsv | cut -f 1 | sort -u`
        - `tail -n +2 data/demo-sort.tsv | cut -f 1 | sort -u | wc -l`
- multiples categories: `uniq --count`
    - how many genes on each chromosome?
        - `tail -n +2 data/demo-sort.tsv | cut -f 1,4` (warning: there are duplicates)
        - `tail -n +2 data/demo-sort.tsv | cut -f 1,4 | sort -k2,2 -k1,1` (warning: there are duplicates)
        - `tail -n +2 data/demo-sort.tsv | cut -f 1,4 | sort -k2,2 -k1,1 -u`
        - `tail -n +2 data/demo-sort.tsv | cut -f 1,4 | sort -k2,2 -k1,1 -u | cut -f 2`
        - `tail -n +2 data/demo-sort.tsv | cut -f 1,4 | sort -k2,2 -k1,1 -u | cut -f 2 | uniq --count`
        - Note for later: you are writing an ad-hoc query engine!



# TODO

- Using cut, sort, and unique to explore data with bash 
    - https://riffomonas.org/code_club/2020-08-06-exploring-data-with-bash
- File Manipulation: sort and uniq
    - https://riffomonas.org/code_club/2020-08-06-exploring-data-with-bash
- Advanced: use `miller`
    - https://github.com/johnkerl/miller

