# TODO 2023-2024

- aggregation (`GROUP BY`) and associated operations (`MIN`, `MAX`, `SUM`, `AVERAGE`,...)
- nested queries
- negation
- [normalization](https://en.wikipedia.org/wiki/Database_normalization)
