# Retrieve data

- https://ftp.ensembl.org/pub/current_gff3/homo_sapiens/
- GFF3 specification
    - https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md
    - https://www.ensembl.org/info/website/upload/gff3.html


# Explore

- number of lines
- beginning (all the lines starting with `#`)
    - are they all at the beginning ?
        - TODO: add explanation for `grep`
- comments
- columns and their signification


# Prepare

- remove comments
- subset of lines
    - challenge: only display the 4th line using `head` and `tail`


# Field category

- types (column 3)
- `sort` on text
- `sort` on numbers
- number of elements for each category



# Parse attributes field

- `cut --delimiter=";"`
- https://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash


# Compare performances

- compare the performances of head|tail VS tail|head for displaying the kth line of a large file, depending on whether the kth line is at the beginning, the middle or the end of the file
- compare the performances of grep|sort VS sort|grep
- compare the performances with and ad-hoc python script
