# DELiS - tabulated files hands-on


## Question 4

```bash
wc -l Homo_sapiens.GRCh38.108.chromosome.22.gff3
```


## Question 5

```bash
head -n 15 Homo_sapiens.GRCh38.108.chromosome.22.gff3
```


## Question 6

```bash
grep "#" Homo_sapiens.GRCh38.108.chromosome.22.gff3
```


## Question 7

```bash
grep "#" Homo_sapiens.GRCh38.108.chromosome.22.gff3 | wc -l
```


## Question 9

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | wc -l
```


## Question 10

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | head -n 5
```


## Question 11

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | head -n 5 > test-small.gff3
```


## Question 12

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | tail -n 5
```


## Question 14

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | head -n 25 > test-medium.gff3 && grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 | tail -n 25 >> test-medium.gff3
#
# control:
# wc -l test-medium.gff3
#
# solution en 1 ligne:
# grep -v '#' Homo_sapiens.GRCh38.108.chromosome.22.gff3 | (head -n25 ; tail -n25) > test-medium.gff3
```


## Question 15

```bash
grep "#" --invert-match Homo_sapiens.GRCh38.108.chromosome.22.gff3 > test-large.gff3
# control:
# wc -l test-large.gff3
```


## Question 16

```bash
cut -f 1 test-large.gff3
```


## Question 17

```bash
cut -f 1 test-large.gff3  | sort | uniq
```


## Question 18

```bash
cut -f 3 test-large.gff3  | sort | uniq
```


## Question 19

```bash
cut -f 3 test-large.gff3  | grep exon | wc -l
```


## Question 20

```bash
cut -f 3 test-large.gff3  | sort | uniq | grep gene
#cut -f 3 test-large.gff3  | sort | uniq | grep gene | wc -l
```


## Question 21

```bash
cut -f 3 test-large.gff3  | grep gene | wc -l
```


## Question 22

```bash
cut -f 3 test-large.gff3  | grep --line-regexp gene | wc -l
```


## Question 23

```bash
cut -f 3 test-large.gff3  | sort | uniq --count
```


## Question 24

```bash
sort -k3,3 test-large.gff3
```
