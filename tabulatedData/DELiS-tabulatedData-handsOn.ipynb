{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "---\n",
    "title: Data Engineering in Life Sciences: tabulated data\n",
    "tags: DELiS, tabulatedData, bash\n",
    "lang: en\n",
    "version: 1.3\n",
    "date: 2024-10-11\n",
    "---\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Objective"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This session aims at becoming familiar with the main GNU/linux commands for manipulating tabulated files."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Support files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Jupyter bash_kernel\n",
    "    - `pip3 install --user bash_kernel`\n",
    "    - `python3 -m bash_kernel.install --user`\n",
    "    - (re-) start jupyter\n",
    "- Files for the hands-on session\n",
    "    - https://gitlab.com/odameron/supportDELiS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. GFF: file format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**GFF** (*Generic Feature Format*) is a classic file format for describing genes and other features of DNA, RNA and protein sequences. Its specification is available at: https://www.ensembl.org/info/website/upload/gff3.html There is also a detailed explanation at https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md\n",
    "\n",
    "GFF principles:\n",
    "- plain text\n",
    "- comment lines start with `#`\n",
    "- the first line **must** be `##gff-version 3`\n",
    "- 9 columns, tab-delimited\n",
    "- empty fields are represented by the `.` character\n",
    "\n",
    "GFF fields:\n",
    "1. **seqid** - name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. \n",
    "2. **source** - name of the program that generated this feature, or the data source (database or project name). Free text\n",
    "3. **type** - type of feature. Must be a term or accession from the SOFA sequence ontology. This is constrained to be either a term from the Sequence Ontology (e.g. `region`) or an SO accession number (e.g. `SO:0000001`). The latter alternative is distinguished using the syntax `SO:000000`. In either case, it must be `sequence_feature` (`SO:0000110`) or one of its descendents (explore the hierarchy with http://www.sequenceontology.org/browser/current_svn/term/SO:0000110).\n",
    "4. **start** - Start position of the feature, with sequence numbering starting at 1.\n",
    "5. **end** - End position of the feature, with sequence numbering starting at 1.\n",
    "6. **score** - A floating point value.\n",
    "7. **strand** - defined as + (forward) or - (reverse).\n",
    "8. **phase** - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..\n",
    "9. **attributes** - A semicolon-separated list of tag-value pairs, providing additional information about each feature. Some of these tags are predefined, e.g. ID, Name, Alias, Parent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Explore GFF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Download a GFF file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- the latest version (currently 108) is available from https://ftp.ensembl.org/pub/current_gff3/\n",
    "- this is actually a link to the release-specific directory https://ftp.ensembl.org/pub/release-108/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 1:** Download a file from https://ftp.ensembl.org/pub/current_gff3/homo_sapiens/ (take a small one, e.g. chromosome22)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "--2024-10-10 12:33:04--  https://ftp.ensembl.org/pub/current_gff3/homo_sapiens/Homo_sapiens.GRCh38.112.chromosome.22.gff3.gz\n",
      "Resolving ftp.ensembl.org (ftp.ensembl.org)... 193.62.193.169\n",
      "Connecting to ftp.ensembl.org (ftp.ensembl.org)|193.62.193.169|:443... connected.\n",
      "HTTP request sent, awaiting response... 200 OK\n",
      "Length: 1002420 (979K) [application/x-gzip]\n",
      "Saving to: ‘Homo_sapiens.GRCh38.112.chromosome.22.gff3.gz’\n",
      "\n",
      "Homo_sapiens.GRCh38 100%[===================>] 978,93K  1,32MB/s    in 0,7s    \n",
      "\n",
      "2024-10-10 12:33:05 (1,32 MB/s) - ‘Homo_sapiens.GRCh38.112.chromosome.22.gff3.gz’ saved [1002420/1002420]\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# The latest release number might not be 112 anymore\n",
    "# Adapt the following commands accordingly (after uncommenting them) \n",
    "#\n",
    "mkdir -p ~/ontology/ensembl/release112\n",
    "cd ~/ontology/ensembl/release112\n",
    "\n",
    "# uncomment and execute the following lines\n",
    "wget https://ftp.ensembl.org/pub/current_gff3/homo_sapiens/Homo_sapiens.GRCh38.112.chromosome.22.gff3.gz\n",
    "gunzip Homo_sapiens.GRCh38.112.chromosome.22.gff3.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Explore"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 2:** Use `ls -l` to get an idea of the file characteristics. What is the size of the file (in bytes)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-rw-rw-r-- 1 olivier olivier 11472657 Mar  5  2024 /home/olivier/ontology/ensembl/release112/Homo_sapiens.GRCh38.112.chromosome.22.gff3\n"
     ]
    }
   ],
   "source": [
    "ls -l ~/ontology/ensembl/release112/Homo_sapiens.GRCh38.112.chromosome.22.gff3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 3:** Use `du <filename>` (disk usage) and `du -h <filename>` to get a sensible idea of the file size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "11204\tHomo_sapiens.GRCh38.112.chromosome.22.gff3\n"
     ]
    }
   ],
   "source": [
    "du Homo_sapiens.GRCh38.112.chromosome.22.gff3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "11M\tHomo_sapiens.GRCh38.112.chromosome.22.gff3\n"
     ]
    }
   ],
   "source": [
    "du -h Homo_sapiens.GRCh38.112.chromosome.22.gff3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 4:** How many lines?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# answer: 74510"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 5:** From question 4, we see that the file is quite large. Use the appropriate command to look at the beginning of the file. Observe that there are many lines that are actually comments. Increase the number of lines to display until you see a few lines of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display the beginning of the file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 6:** It looks like there are some comment lines in the file. Are they all at the beginning, or are there other comments further down? To find out use the `grep` command to retrieve all the lines that contain the character `#` (since this is also a special bash character, you will need to use quotes: `\"#\"`). Give a quick look at `man grep` to get an overview of how to use the `grep` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# list the lines that contain \"#\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 7:** Combine the previous command with another one to find the number of comment lines in the file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# answer: 1453"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 8:** From question 4 and question 7, how many lines actually contain information? (just make the substraction in your head, you do not have to use a shell command for this)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# answer: 74549 - 1453 = 73096"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 9:** using `man grep`, find a convenient argument that will allow you to retrieve the lines that **do not** contain `\"#\"`. Combine the result with another command to count them. Is it consistent with your answer from question 8?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# answer: 73096"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Prepare a test file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 10:** Adapt the result from question 9 in order to display the first 5 lines that are not comments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and display the first 5 lines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 11:** Adapt the result from the previous question to save the result into the file `test-small.gff3`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and save the first 5 lines into test-small.gff3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 12:** Adapt the result from question 9 in order to display the *last* 5 lines that are not comments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and display the last 5 lines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 13:** Adapt the result from the previous question to display the first 25 and the last 25 lines that are not comments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and display the first 25 and the last 25 lines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 14:** Adapt the result from the previous question to save the first 25 and the last 25 lines that are not comments into the file `test-medium.gff3`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and write the first 25 and the last 25 lines into test-medium.gff3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 15:** Save all the lines that are not comments into `test-large.gff3` (yes, it would have been smarter to do this first and *then* use it to create `test-small.gff3` and `test-medium.gff3`, but the point was to get you used to combining multiples commands :-) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove the comments and write the result into test-large.gff3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the remainder of the session, we will use: \n",
    "- `test-small.gff3` to have a quick overview of the file structure when writing queries\n",
    "- `test-medium.gff3` to iteratively check whether the queries produce the expected results \n",
    "- `test-large.gff3` to perform the whole computation on a large file once you are pretty confident it will yield the correct result (even if it takes some time)\n",
    "\n",
    "Of course, this makes sense because we are going to reuse these files several times, so that removing the headers and the comments is only done once. In the situations where you only need to process the file once, using such intermediate files is unnecessary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Analyze the categories"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this section, we call a category the value of a cell (usually a string, although it may also be a number) that is present in 0, 1 or many cells of a column. For example, `\"biological_region\"` or `\"exon\"` are categories in column 3 (type)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 16:** display only column 1 (seqid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display column 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 17:** What are the different values in column 1 (seqid) in `test-large.gff3`? Is it surprising? How do you interpret these values? What do you expect if you run the same command on `Homo_sapiens.GRCh38.108.gff3` instead of `Homo_sapiens.GRCh38.108.chromosome.22.gff3` (theoretical question, you may not want to actually try)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "# different values in column 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 18:** What are the different values of column 3 (type)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "# different values in column 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 19: counting a category the painful way** We now want to find how many lines are of type exon. Of course, the string `\"exon\"` may be present in several places in a line (e.g. in column 9), so doing a `grep exon` on the whole file would return false positives. Write a one-line command combining `cut`, `grep` and `wc` that counts the number of exons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of exons (the hard way)\n",
    "#\n",
    "# answer: 34446"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 20:** Unfortunately, the previous approach only works because the string `\"exon\"` is only present in a single category. Looking at the output from question 18, how many categories contain the string `\"gene\"`? Write a combination of commands that displays only the categories from column 3 that contain the string `\"gene\"`. Complete this combination to compute the number of categories from column 3 that contain the string `\"gene\"`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "# list and then count the occurrences of types containing the string \"gene\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 21:** Compute the number of lines for which the type contains the string `\"gene\"`. Notice that is *not* the number of genes in chromosome 22, as it aggregates the genes, the pseudogene,..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of lines for which the type contains \"gene\" \n",
    "# WARNING: != number of genes\n",
    "#\n",
    "# answer: 1493"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 22:** Look at the `--line-regexp` option of the command `grep`. How many genes are there actually on chromosome 22?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of genes on chromosome 22\n",
    "#\n",
    "# answer: 505"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 23: Counting the categories the nice and easy way** Look at the man page of `uniq` (hint: around the `--count` option) and adapt the answer to question 18 in order to display the  number of occurrences of each category. Check that you find the same number of genes as before"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of occurrences of each type"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Sort the lines"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 24:** Sort the lines by alphabetical order of type (column3). NB: remember that `sort -k3` sorts by the third field until the end of the line, so you need to restrict the range of the sorting operation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "# lines sorted by type (column 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 25:** Sort the lines by start position (column 4) by only changing the fild number. What is wrong?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# lines sorted by start position (column 4), WRONG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 26:** Sort the lines by start position (column 4), taking into account its numerical nature"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "# lines sorted by start position (column 4), RIGHT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that at this point, we would naturally want to combine filtering and sorting (e.g. sort all the exons by starting position), but we are (already) reaching the limit of the command-based approach.\n",
    "\n",
    "From there, we need `awk`, which is a query language by itself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 27:** Use `awk` to print all the lines where field 3 is `\"exon\"`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22\tensembl\texon\t10736171\t10736283\t.\t-\t.\tParent=transcript:ENST00000615943;Name=ENSE00003736336;constitutive=1;ensembl_end_phase=-1;ensembl_phase=-1;exon_id=ENSE00003736336;rank=1;version=1\n",
      "22\thavana\texon\t10939388\t10939423\t.\t-\t.\tParent=transcript:ENST00000635667;Name=ENSE00003790077;constitutive=1;ensembl_end_phase=-1;ensembl_phase=-1;exon_id=ENSE00003790077;rank=9;version=1\n",
      "22\thavana\texon\t10940597\t10940707\t.\t-\t.\tParent=transcript:ENST00000635667;Name=ENSE00003791492;constitutive=1;ensembl_end_phase=-1;ensembl_phase=-1;exon_id=ENSE00003791492;rank=8;version=1\n"
     ]
    }
   ],
   "source": [
    "# in awk:\n",
    "# - $1 is first field, \n",
    "# - $2 is... 2nd field :-)\n",
    "# - ...\n",
    "# - $0 is all the fields\n",
    "\n",
    "awk '$3 == \"exon\" {print $0}' test-large.gff3 | head -n 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Question 28:** Use `awk` to print the fields 1, 3, 4 and 5 of the lines where field 3 is `\"exon\"`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22\texon\t10736171\t10736283\n",
      "22\texon\t10939388\t10939423\n",
      "22\texon\t10940597\t10940707\n"
     ]
    }
   ],
   "source": [
    "awk '$3 == \"exon\" {print $1\"\\t\"$3\"\\t\"$4\"\\t\"$5}' test-large.gff3 | head -n 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6. Parse attributes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that column 9 is actually a list of fields, separated by `\";\"`. The fields that have multiple values are quoted, and their values are separated by commas.\n",
    "Parsing column 9 (with `cut --delimiter=\";\"`) then becomes particularly painful.\n",
    "\n",
    "This situation highlights three points:\n",
    "- choosing a fixed number of columns is probably not a good idea\n",
    "- refering to the columns by their position instead of using headers is not compatible with the flexibility permited by column 9\n",
    "- the tabulated format is not adapted to some data structures, such as relations or hierarchies\n",
    "\n",
    "Obviously, other formalisms are needed. The principal ones will be covered in the following lessons."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 7. Challenges (optional): performances comparison"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Challenge 1:** compare the performances of `head | tail` VS `tail | head` for displaying the kth line of a large file, depending on whether the kth line is at the beginning, the middle or the end of the file. Use `time someCommand arg1 arg2... argn` for timing the execution of `someCommand`, and `time { someCommand arg1 arg2... argn | someOtherCommand ;}` for timing a piped combination of commands (Notice the curly braces `\"{\"` `\"}\"`, and the final semi-colon `\";\"`) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Challenge 2:** if you want to retrieve only some of the lines of a file and have them sorted, compare perfoming the line selection before VS after sorting (e.g. to selecty the exons and sort them by their end position)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Challenge 3:** write two python scripts performing the same task as Challenge 1, and compare their performances with the ones from Challenge 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
