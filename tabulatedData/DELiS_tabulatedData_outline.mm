<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="DELiS&#xa;tabulated data" FOLDED="false" ID="ID_1663637249" CREATED="1673305948406" MODIFIED="1673306052832" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<node TEXT="How to represent and query structured data?" POSITION="right" ID="ID_1513532422" CREATED="1673337462231" MODIFIED="1673337493837">
<icon BUILTIN="help"/>
<edge COLOR="#00007c"/>
<font BOLD="true"/>
</node>
<node TEXT="explore the file(s) content" POSITION="right" ID="ID_1378062661" CREATED="1673306100080" MODIFIED="1673306106652">
<edge COLOR="#ff0000"/>
<node TEXT="print the whole file" ID="ID_67166220" CREATED="1673306107968" MODIFIED="1673306119980">
<node TEXT="cat" ID="ID_1787391428" CREATED="1673306121150" MODIFIED="1673306178970">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="print the beginning of the file" ID="ID_827365757" CREATED="1673306187336" MODIFIED="1673306194741">
<node TEXT="head" ID="ID_135046647" CREATED="1673306208352" MODIFIED="1673306221542">
<font NAME="Courier"/>
</node>
<node TEXT="from the beginning" ID="ID_530053370" CREATED="1673306223976" MODIFIED="1673306228931"/>
<node TEXT="up to a certain position" ID="ID_233486674" CREATED="1673306229296" MODIFIED="1673306242522">
<node TEXT="determined from the beginning" ID="ID_1739403071" CREATED="1673306244824" MODIFIED="1673306252795"/>
<node TEXT="determined from the end" ID="ID_1240387537" CREATED="1673306253064" MODIFIED="1673306257699"/>
</node>
</node>
<node TEXT="print the end of the file" ID="ID_1395837913" CREATED="1673306195032" MODIFIED="1673306199374">
<node TEXT="tail" ID="ID_1807291543" CREATED="1673306263576" MODIFIED="1673308328052">
<font NAME="Courier"/>
</node>
<node TEXT="from a certain position" ID="ID_1983401123" CREATED="1673306266368" MODIFIED="1673306274691">
<node TEXT="determined from the beginning" ID="ID_1578104954" CREATED="1673306282136" MODIFIED="1673306287787"/>
<node TEXT="determined from the end" ID="ID_1471673981" CREATED="1673306288039" MODIFIED="1673306292812"/>
</node>
<node TEXT="to the end" ID="ID_1105234660" CREATED="1673306275048" MODIFIED="1673306280646"/>
</node>
<node TEXT="count" ID="ID_505226688" CREATED="1673307052993" MODIFIED="1673307060565">
<node TEXT="the number of words" ID="ID_1984365464" CREATED="1673307062881" MODIFIED="1673307069244"/>
<node TEXT="the number of lines" ID="ID_395817214" CREATED="1673307069698" MODIFIED="1673307082877"/>
</node>
</node>
<node TEXT="concatenate several files" POSITION="right" ID="ID_879780495" CREATED="1673306794096" MODIFIED="1673306803050">
<edge COLOR="#0000ff"/>
<node TEXT="one after the other" ID="ID_1374412791" CREATED="1673307656147" MODIFIED="1673307665246">
<node TEXT="cat" ID="ID_821889701" CREATED="1673306803055" MODIFIED="1673306804460"/>
<node TEXT="may require to remove the header (cf. removing lines)" ID="ID_1735232218" CREATED="1673306804905" MODIFIED="1673306853141"/>
</node>
<node TEXT="side by side" ID="ID_813628906" CREATED="1673307673003" MODIFIED="1673307676477">
<node TEXT="paste" ID="ID_955601230" CREATED="1673307678410" MODIFIED="1673307680575"/>
<node TEXT="cf. section on join" ID="ID_1399959495" CREATED="1673307876764" MODIFIED="1673307883054"/>
</node>
</node>
<node TEXT="select the relevant column(s)" POSITION="right" ID="ID_299285793" CREATED="1673307103137" MODIFIED="1673308458474">
<edge COLOR="#00ff00"/>
<node TEXT="cut" ID="ID_748353905" CREATED="1673307121201" MODIFIED="1673307123005"/>
<node TEXT="extract 1 column" ID="ID_734479969" CREATED="1673307146928" MODIFIED="1673307153741"/>
<node TEXT="extract several columns" ID="ID_1973350156" CREATED="1673307154123" MODIFIED="1673307161221"/>
</node>
<node TEXT="select the relevant lines" POSITION="right" ID="ID_1655619999" CREATED="1673307719571" MODIFIED="1673307724238">
<edge COLOR="#ff00ff"/>
<node TEXT="head" ID="ID_1454795201" CREATED="1673307752955" MODIFIED="1673307756647"/>
<node TEXT="tail" ID="ID_1764150865" CREATED="1673307757091" MODIFIED="1673307758883"/>
<node TEXT="grep" ID="ID_259936612" CREATED="1673307759275" MODIFIED="1673307762005"/>
<node TEXT="uniq" ID="ID_224138342" CREATED="1673307762682" MODIFIED="1673307764207"/>
</node>
<node TEXT="reorder columns" POSITION="right" ID="ID_1221239232" CREATED="1673308811493" MODIFIED="1673309087402">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="reorder lines" POSITION="right" ID="ID_188015229" CREATED="1673309088198" MODIFIED="1673309091760">
<edge COLOR="#7c0000"/>
</node>
</node>
</map>
