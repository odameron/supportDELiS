{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "---\n",
    "title: Data Engineering in Life Sciences: hierarchical data\n",
    "tags: DELiS, hierarchicalData, JSON, XML, HTML, markup\n",
    "lang: en\n",
    "version: 1.3\n",
    "date: 2025-02-25\n",
    "---\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Objective"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This session aims at becoming familiar with the data representation formats that are adapted to representing nested/hierarchical information, and the associated querying/processing languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Support files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The latest version of this file and material for the associated hands-on session are available at https://gitlab.com/odameron/supportDELiS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. JSON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 JSON principles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Specification: [https://www.json.org/](https://www.json.org/)\n",
    "- JavaScript Object Notation\n",
    "    - text-based\n",
    "    - originally designed to serialize (i.e. represent by a string) JavaScript objects\n",
    "    - actually useful in a larger context\n",
    "        - data exchange\n",
    "        - language-independent, can be usde by most sensible programming languages\n",
    "- file extension: `.json`\n",
    "- MIME type: `application/json`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.1 A JSON document is composed of 1 value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> A JSON document is composed of exactly 1 element, that can be either:\n",
    "> - a primitive value\n",
    "> - a composed value:\n",
    ">     - an array\n",
    ">     - an object"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.2 Primitive values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> A primitive value can be either:\n",
    "> - the `null` keyword that represents an empty value\n",
    "> - the `true` or `false` keywords that represents booleans\n",
    "> - a number possibly in scientific notation\n",
    "> - string delimited by double quotes (double quotes within the string must be escaped with a backslash)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NB:** `null`, `true`, `false` (keywords) != `\"null\"`, `\"true\"`, `\"false\"` (strings)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.3 Arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> An **array** is an *ordered list* of zero, one or several comma-separated elements.\n",
    "> It is delimited by square brackets `[` and `]`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.4 Objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> An **object** is an *unordered set* of zero, one or more string:element pairs separated by commas. \n",
    "> The strings are unique for the object (**FIXME**: is this really true? Surprisingly does not appear to be in the specs).\n",
    "> The  object is delimited by curly braces `{` and `}`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1.5 Comments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> JSON is data-only, there are no comments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example representation of a gene as a flat structured object. Note that there is no order between the (key, value) pairs (contrary to the columns order in a tabulated file).\n",
    "\n",
    "```json\n",
    "{\n",
    "  \"type\": \"gene\",\n",
    "  \"ident\": \"gene1\",\n",
    "  \"start\": 5,\n",
    "  \"end\": 12,\n",
    "  \"chromosome\": \"chr1\"\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example representation of chromosome as an *ordered* collection of genes (an you can choose to ignore this order on processing).\n",
    "\n",
    "NB1: we use an array to represent the collection of genes because an object would have required keys.\n",
    "NB2: note that compared to the previous example, since genes are contained into a chromosome object, we can ommit the `\"chromosome\"` key.\n",
    "\n",
    "```json\n",
    "{\n",
    "  \"type\": \"chromosome\",\n",
    "  \"ident\": \"chr1\",\n",
    "  \"regions\": [\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene1\",\n",
    "      \"start\": 5,\n",
    "      \"end\": 12\n",
    "    },\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene3\",\n",
    "      \"start\": 17,\n",
    "      \"end\": 63\n",
    "    }\n",
    "  ]\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Exercice:** Propose a JSON representation for a chromosome where the collection of genes is represented by an object which keys are the gene identifiers. Use an online validator to check that your solution is syntactically-valid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```bash\n",
    "# SOLUTION:\n",
    "echo \"ewogICJ0eXBlIjogImNocm9tb3NvbWUiLAogICJpZGVudCI6ICJjaHIxIiwKICAicmVnaW9ucyI6IHsKICAgICJnZW5lMSI6IHsKICAgICAgInR5cGUiOiAiZ2VuZSIsCiAgICAgICJzdGFydCI6IDUsCiAgICAgICJlbmQiOiAxMgogICAgfSwKICAgICJnZW5lMyI6IHsKICAgICAgInR5cGUiOiAiZ2VuZSIsCiAgICAgICJzdGFydCI6IDE3LAogICAgICAiZW5kIjogNjMKICAgIH0KICB9Cn0K\" | base64 --decode\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Reading and writing JSON in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> In Python, `json` is a standard module.\n",
    "> \n",
    "> Reading JSON can be achieved\n",
    "> - from a file with `json.load(...)` (without an 's')\n",
    "> - from a string with `json.loads(...)` (with an 's')\n",
    "> \n",
    "> Writing JSON can be achieved\n",
    "> - to a file with `json.dump(...)` (without an 's')\n",
    "> - to a string with `json.dumps(...)` (with an 's')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.1 Pretty-print a JSON file from a terminal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```bash\n",
    "echo '{\"json\":\"obj\", \"someField\":42, \"someOtherField\": \"some value\"}' | python3 -m json.tool\n",
    "```\n",
    "\n",
    "Result:\n",
    "```bash\n",
    "{\n",
    "    \"json\": \"obj\",\n",
    "    \"someField\": 42,\n",
    "    \"someOtherField\": \"some value\"\n",
    "}\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.2 Read JSON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.2.1 from a string with `json.loads(...)` (with an 's')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "myData = \"\"\"\n",
    "{\n",
    "  \"type\": \"chromosome\",\n",
    "  \"ident\": \"chr1\",\n",
    "  \"regions\": [\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene1\",\n",
    "      \"start\": 5,\n",
    "      \"end\": 12\n",
    "    },\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene3\",\n",
    "      \"start\": 17,\n",
    "      \"end\": 63\n",
    "    }\n",
    "  ]\n",
    "}\n",
    "\"\"\"\n",
    "\n",
    "myObjectFromString = json.loads(myData)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'dict'>\n"
     ]
    }
   ],
   "source": [
    "print(type(myObjectFromString))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "==========\n",
      "type\n",
      "\n",
      "chromosome\n",
      "==========\n",
      "ident\n",
      "\n",
      "chr1\n",
      "==========\n",
      "regions\n",
      "\n",
      "[{'type': 'gene', 'ident': 'gene1', 'start': 5, 'end': 12}, {'type': 'gene', 'ident': 'gene3', 'start': 17, 'end': 63}]\n"
     ]
    }
   ],
   "source": [
    "for (k, v) in myObjectFromString.items():\n",
    "    print(\"=\"*10)\n",
    "    print(k)\n",
    "    print()\n",
    "    print(v)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "chr1\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromString[\"ident\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'type': 'gene', 'ident': 'gene3', 'start': 17, 'end': 63}\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromString[\"regions\"][1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Exercice:** Note that with this representation of a chromosome, the `regions` value is an array, so we have to access its elements by their position. Use your representation as an object to print `myObjectFromString[\"regions\"][\"gene3\"]`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'type': 'gene', 'start': 17, 'end': 63}\n"
     ]
    }
   ],
   "source": [
    "import json\n",
    "\n",
    "myData = \"\"\"\n",
    "{\n",
    "  \"type\": \"chromosome\",\n",
    "  \"ident\": \"chr1\",\n",
    "  \"regions\": {\n",
    "    \"gene1\": {\n",
    "      \"type\": \"gene\",\n",
    "      \"start\": 5,\n",
    "      \"end\": 12\n",
    "    },\n",
    "    \"gene3\": {\n",
    "      \"type\": \"gene\",\n",
    "      \"start\": 17,\n",
    "      \"end\": 63\n",
    "    }\n",
    "  }\n",
    "}\n",
    "\n",
    "\"\"\"\n",
    "\n",
    "myObjectFromString2 = json.loads(myData)\n",
    "print(myObjectFromString2[\"regions\"][\"gene3\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.2.2 from a file with `json.load(...)` (without 's')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "with open('./data/chromosome.json') as jsonFile:\n",
    "    myObjectFromFile = json.load(jsonFile)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'dict'>\n"
     ]
    }
   ],
   "source": [
    "print(type(myObjectFromFile))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "==========\n",
      "type\n",
      "\n",
      "chromosome\n",
      "==========\n",
      "ident\n",
      "\n",
      "chr1\n",
      "==========\n",
      "regions\n",
      "\n",
      "[{'type': 'gene', 'ident': 'gene1', 'start': 5, 'end': 12}, {'type': 'gene', 'ident': 'gene3', 'start': 17, 'end': 63}]\n"
     ]
    }
   ],
   "source": [
    "for (k, v) in myObjectFromFile.items():\n",
    "    print(\"=\"*10)\n",
    "    print(k)\n",
    "    print()\n",
    "    print(v)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3.3 Write JSON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.3.1 to a string with `json.dumps(...)` (with an 's')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{\"type\": \"chromosome\", \"ident\": \"chr1\", \"regions\": [{\"type\": \"gene\", \"ident\": \"gene1\", \"start\": 5, \"end\": 12}, {\"type\": \"gene\", \"ident\": \"gene3\", \"start\": 17, \"end\": 63}]}\n",
      "\n",
      "{\n",
      "  \"type\": \"chromosome\",\n",
      "  \"ident\": \"chr1\",\n",
      "  \"regions\": [\n",
      "    {\n",
      "      \"type\": \"gene\",\n",
      "      \"ident\": \"gene1\",\n",
      "      \"start\": 5,\n",
      "      \"end\": 12\n",
      "    },\n",
      "    {\n",
      "      \"type\": \"gene\",\n",
      "      \"ident\": \"gene3\",\n",
      "      \"start\": 17,\n",
      "      \"end\": 63\n",
      "    }\n",
      "  ]\n",
      "}\n"
     ]
    }
   ],
   "source": [
    "import json\n",
    "\n",
    "with open('./data/chromosome.json') as jsonFile:\n",
    "    myObjectFromFile = json.load(jsonFile)\n",
    "\n",
    "print(json.dumps(myObjectFromFile))\n",
    "print()\n",
    "print(json.dumps(myObjectFromFile, indent=\"  \"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3.3.2 to a file with `json.dump(...)` (without an 's')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "myData = \"\"\"\n",
    "{\n",
    "  \"type\": \"chromosome\",\n",
    "  \"ident\": \"chr1\",\n",
    "  \"regions\": [\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene1\",\n",
    "      \"start\": 5,\n",
    "      \"end\": 12\n",
    "    },\n",
    "    {\n",
    "      \"type\": \"gene\",\n",
    "      \"ident\": \"gene3\",\n",
    "      \"start\": 17,\n",
    "      \"end\": 63\n",
    "    }\n",
    "  ]\n",
    "}\n",
    "\"\"\"\n",
    "\n",
    "myObjectFromString = json.loads(myData)\n",
    "\n",
    "with open('./data/chromosome-exported.json', 'w') as jsonFile:\n",
    "    myObjectFromFile = json.dump(myObjectFromString, jsonFile)\n",
    "    \n",
    "with open('./data/chromosome-exported-indented.json', 'w') as jsonFile:\n",
    "    myObjectFromFile = json.dump(myObjectFromString, jsonFile, indent=\"  \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Idea:** jupyter notebooks are actually represented in json. Inspect this file with a text editor."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.4 process JSON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- process the python representation of the object\n",
    "- or use XQuery ([https://www.w3.org/TR/xquery-31/](https://www.w3.org/TR/xquery-31/)), which was designed for XML (see below), but also works with JSON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. YAML"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 YAML principles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- specification: [https://yaml.org/](https://yaml.org/)\n",
    "- recursive acronym for “YAML Ain’t Markup Language”\n",
    "- commonly used for configuration files\n",
    "- extension of JSON (every JSON file is also a YAML file)\n",
    "    - uses Python-style whitespace indentation to indicate nesting\n",
    "    - comments (from `#` until the end of the line), \n",
    "    - extensible data types, \n",
    "    - relational anchors, \n",
    "    - strings without quotation marks, \n",
    "    - mapping types preserving key order.\n",
    "- file extension: `.yaml`\n",
    "- MIME type: `application/yaml`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1.1 Documents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A file can contain multiple documents separated by three hyphens `---`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1.2 Lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2 possible syntaxes:\n",
    "- comma+space separated items, delimited by `[` and `]`\n",
    "- indented block, one item per line, each item starts with dash+space (`\"- \"`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "[\"first item\", \"second item\", \"third item\"]\n",
    "\n",
    "# is equivalent to:\n",
    "- \"first item\"\n",
    "- \"second item\"\n",
    "- \"third item\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1.3 Associative arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2 possible syntaxes:\n",
    "- comma+space separated entries, delimited by `{` and `}`, each entry is a key, a colon, a space and a value\n",
    "- indented block, one entry per line, each line is is a key, a colon, a space and a value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "{type: gene, ident: gene1, start: 5, end: 12}\n",
    "\n",
    "# is equivalent to:\n",
    "type: gene\n",
    "ident: gene1\n",
    "start: 5\n",
    "end: 12\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Exercice:** the file `./data/chromosome.yaml` represents the information from `./data/chromosome.tsv` with the regions of a chromosome are an (ordered) list of genes. Create a `chromosome-array.yaml` file where the regions are an associative array of genes, where the keyrs are the genes identifiers. Check that your file is syntactically-valid with an online YAML validator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1.4 Anchors and references for linking nodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- a **node anchor** identifies a node\n",
    "    - starts with an ampersand `&` followed by the node identifier\n",
    "    - anchor names must not contain the `\"[\"`, `\"]\"`, `\"{\"`, `\"}\"` and `\",\"` characters.\n",
    "- a **node reference** points to the anchor of a node\n",
    "    - starts with a star `*` followed by the node identifier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "ident: chr1\n",
    "type: chromosome\n",
    "regions:\n",
    "    - &identGene1\n",
    "      ident: gene1\n",
    "      type: gene\n",
    "      start: 5\n",
    "      end: 12\n",
    "    - &identGene2\n",
    "      ident: gene3\n",
    "      type: gene\n",
    "      start: 17\n",
    "      end: 63\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Exercice:** add a new regions for chromosome `chr1`, which is of type locus, starts at position 4 and ends at position 70. Give this region an identifier and an anchor (which value does not have to be the same as its identifier) and a `contains` field that refers to gene1 and gene3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Reading and writing YAML in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- YAML is not part of the standard Python library\n",
    "    - ruamel.yaml ([https://pypi.python.org/pypi/ruamel.yaml](https://pypi.python.org/pypi/ruamel.yaml)) is an update of pyYAML\n",
    "        - debian package: `python3-ruamel.yaml` (`sudo apt-get update && sudo apt-get install python3-ruamel.yaml`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ruamel.yaml import YAML\n",
    "\n",
    "yaml=YAML()\n",
    "with open('./data/chromosome.yaml') as yamlFile:\n",
    "    myObjectFromFile = yaml.load(yamlFile)\n",
    "\n",
    "myObjectFromString = yaml.load(\"\"\"\n",
    "ident: chr1\n",
    "type: chromosome\n",
    "regions:\n",
    "    - ident: gene1\n",
    "      type: gene\n",
    "      start: 5\n",
    "      end: 12\n",
    "    - ident: gene3\n",
    "      type: gene\n",
    "      start: 17\n",
    "      end: 63\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'ruamel.yaml.comments.CommentedMap'>\n",
      "\n",
      "<class 'ruamel.yaml.comments.CommentedMap'>\n"
     ]
    }
   ],
   "source": [
    "print(type(myObjectFromFile))\n",
    "print()\n",
    "print(type(myObjectFromString))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ordereddict([('ident', 'chr1'), ('type', 'chromosome'), ('regions', [ordereddict([('ident', 'gene1'), ('type', 'gene'), ('start', 5), ('end', 12)]), ordereddict([('ident', 'gene3'), ('type', 'gene'), ('start', 17), ('end', 63)])])])\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromFile)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ordereddict([('ident', 'chr1'), ('type', 'chromosome'), ('regions', [ordereddict([('ident', 'gene1'), ('type', 'gene'), ('start', 5), ('end', 12)]), ordereddict([('ident', 'gene3'), ('type', 'gene'), ('start', 17), ('end', 63)])])])\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromString)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "chr1\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromFile['ident'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ordereddict([('ident', 'gene3'), ('type', 'gene'), ('start', 17), ('end', 63)])\n"
     ]
    }
   ],
   "source": [
    "print(myObjectFromFile['regions'][1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Exercice:** like with JSON, use `chromosome-array.yaml` to display `myObjectFromFile['regions']['gene3']['start']`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Markup languages (XML, HTML,...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```xml\n",
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n",
    "<chromosome ident=\"chr1\">\n",
    "    <gene>\n",
    "        <ident>gene1</ident>\n",
    "        <start>5</start>\n",
    "        <end>12</end>\n",
    "    </gene>\n",
    "    <gene ident=\"gene3\" start=\"17\" end=\"63\"></gene>\n",
    "</chromosome> <!-- ici un commentaire -->\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
