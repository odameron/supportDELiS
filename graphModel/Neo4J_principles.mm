<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Neo4J_principles" FOLDED="false" ID="ID_763260911" CREATED="1677249005399" MODIFIED="1677249015907" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="graph = (V, E)" POSITION="right" ID="ID_401047205" CREATED="1677626616849" MODIFIED="1677627002980">
<edge COLOR="#ff00ff"/>
<node TEXT="V = set of nodes (called &quot;vertices&quot;)" ID="ID_1841343485" CREATED="1677626637100" MODIFIED="1677626657521"/>
<node TEXT="E = set of relationships (called &quot;edges&quot;) between elements of V" ID="ID_1465848842" CREATED="1677626658392" MODIFIED="1677626986791">
<node TEXT="directed" ID="ID_805976294" CREATED="1677626703873" MODIFIED="1677626706110"/>
<node TEXT="E included in (VxV)" ID="ID_1608062942" CREATED="1677626706393" MODIFIED="1677626866961"/>
</node>
<node TEXT="nodes and relationships can have attributes of their own" ID="ID_610890753" CREATED="1677626951419" MODIFIED="1677626974223"/>
</node>
<node TEXT="nodes" POSITION="right" ID="ID_762264166" CREATED="1677249025438" MODIFIED="1677249028311">
<edge COLOR="#ff0000"/>
<node TEXT="syntax" ID="ID_787996279" CREATED="1677249032702" MODIFIED="1677249174819">
<node TEXT="()" ID="ID_903448130" CREATED="1677249162648" MODIFIED="1677249169278">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="creation" ID="ID_1543789186" CREATED="1677249068031" MODIFIED="1677249081597">
<node TEXT="CREATE ()" ID="ID_1191977088" CREATED="1677249083192" MODIFIED="1677249153400">
<icon BUILTIN="executable"/>
</node>
<node TEXT="NB: not the create function" ID="ID_1447047319" CREATED="1677249088486" MODIFIED="1677249107698">
<node TEXT="here the parentheses represent the node" ID="ID_299999408" CREATED="1677249107702" MODIFIED="1677249117908"/>
<node TEXT="good practice: space between CREATE and the node" ID="ID_182747911" CREATED="1677249118472" MODIFIED="1677249144115"/>
</node>
</node>
<node TEXT="query" ID="ID_375192550" CREATED="1677249506367" MODIFIED="1677249513723">
<node TEXT="MATCH (n)&#xa;RETURN n" ID="ID_1520680192" CREATED="1677249517440" MODIFIED="1677249575540">
<icon BUILTIN="executable"/>
</node>
<node TEXT="&quot;n&quot; is a (local) name" ID="ID_1866524668" CREATED="1677249578056" MODIFIED="1677250058885">
<node TEXT="assigned during query processing" ID="ID_1210853796" CREATED="1677249929554" MODIFIED="1677250102119"/>
<node TEXT="similar to a variable" ID="ID_1884028517" CREATED="1677250011074" MODIFIED="1677250036677"/>
<node TEXT="its values are successively all the elements that satisfy the pattern (here being a node)" ID="ID_1027531146" CREATED="1677250104722" MODIFIED="1677250115669"/>
</node>
<node TEXT="equivalent to" ID="ID_1820973247" CREATED="1677250143010" MODIFIED="1677250157869">
<node TEXT="match all the elements that are nodes" ID="ID_499181029" CREATED="1677250159290" MODIFIED="1677250174669"/>
<node TEXT="call them &quot;n&quot;" ID="ID_1325295684" CREATED="1677250175434" MODIFIED="1677250179173"/>
<node TEXT="the result of the query are all the possible values for &quot;n&quot;" ID="ID_923785467" CREATED="1677250194698" MODIFIED="1677250212149"/>
</node>
</node>
<node TEXT="identifier" ID="ID_792874992" CREATED="1677249196271" MODIFIED="1677249199163">
<node TEXT="assigned automatically at creation" ID="ID_975342856" CREATED="1677249208174" MODIFIED="1677249221060"/>
<node TEXT="different from the local name &quot;n&quot; in &quot;(n)&quot;" ID="ID_840607970" CREATED="1677627607507" MODIFIED="1677627633285">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="accessible" ID="ID_1823550939" CREATED="1677249267198" MODIFIED="1677627640636">
<node TEXT="by function id()" ID="ID_1256165435" CREATED="1677627640643" MODIFIED="1677627809485">
<font STRIKETHROUGH="true"/>
</node>
<node TEXT="by function elementId()" ID="ID_1785311392" CREATED="1677627645971" MODIFIED="1677627772021">
<node TEXT="returns a node or relationship identifier" ID="ID_358485164" CREATED="1677627772026" MODIFIED="1677627789912"/>
<node TEXT="unique among both nodes and relationships across all databases" ID="ID_50714292" CREATED="1677627790476" MODIFIED="1677627801924"/>
</node>
</node>
<node TEXT="calling CREATE () multiple times create as many nodes (with different identifiers)" ID="ID_1395875596" CREATED="1677249300728" MODIFIED="1677249334043"/>
<node TEXT="MATCH (n)&#xa;RETURN id(n)" ID="ID_1606395525" CREATED="1677249517440" MODIFIED="1677250288618">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="label(s)" ID="ID_1722583455" CREATED="1677249347473" MODIFIED="1677249358948">
<node TEXT="similar to" ID="ID_1058442676" CREATED="1677250610091" MODIFIED="1677250614151">
<node TEXT="category" ID="ID_998986244" CREATED="1677250616316" MODIFIED="1677250620286"/>
<node TEXT="class" ID="ID_306774769" CREATED="1677250620667" MODIFIED="1677250622439"/>
<node TEXT="table name" ID="ID_533649523" CREATED="1677250622819" MODIFIED="1677250626166"/>
</node>
<node TEXT="syntax" ID="ID_65907913" CREATED="1677250417009" MODIFIED="1677250424675">
<node TEXT=":LabelName" ID="ID_1724339572" CREATED="1677250424681" MODIFIED="1677250439607">
<icon BUILTIN="executable"/>
</node>
<node TEXT="recommendation" ID="ID_1187192749" CREATED="1677628116086" MODIFIED="1677628120433">
<node TEXT="camel-case" ID="ID_1273234201" CREATED="1677628121932" MODIFIED="1677628246520"/>
<node TEXT="begins with upper-case caracter" ID="ID_128147626" CREATED="1677628125396" MODIFIED="1677628251282"/>
</node>
</node>
<node TEXT="a node can have 0, 1 or many labels" ID="ID_431817854" CREATED="1677249361936" MODIFIED="1677249374846">
<node TEXT="node with no label: ()" ID="ID_5557394" CREATED="1677250464979" MODIFIED="1677250477980"/>
<node TEXT="node with a label: (:Protein)" ID="ID_1511087201" CREATED="1677250478881" MODIFIED="1677250509918"/>
<node TEXT="node with multiple labels: (:Protein:Enzyme)" ID="ID_1722798791" CREATED="1677250512963" MODIFIED="1677250535567"/>
<node TEXT="usually assign at least 1 label" ID="ID_1434085119" CREATED="1677250756171" MODIFIED="1677250846740">
<icon BUILTIN="idea"/>
</node>
<node TEXT="no order between labels" ID="ID_1759850872" CREATED="1677250791075" MODIFIED="1677250870984">
<icon BUILTIN="idea"/>
</node>
<node TEXT="the 1st label is the &quot;primary label&quot;" ID="ID_203211694" CREATED="1677250770211" MODIFIED="1677250781623"/>
</node>
<node TEXT="query" ID="ID_357365492" CREATED="1677250689155" MODIFIED="1677250695183">
<node TEXT="all the nodes that have a label" ID="ID_82448859" CREATED="1677250942764" MODIFIED="1677250949718">
<node TEXT="MATCH (e:Enzyme)&#xa;RETURN e" ID="ID_642909392" CREATED="1677250880324" MODIFIED="1677250960507">
<icon BUILTIN="executable"/>
</node>
<node TEXT="returns a table" ID="ID_214772192" CREATED="1677251474636" MODIFIED="1677251480233"/>
</node>
<node TEXT="all the nodes that have 2 labels (AND)" ID="ID_332765339" CREATED="1677250966909" MODIFIED="1677250983239">
<node TEXT="MATCH (m:Aldose:Hexose)&#xa;RETURN m" ID="ID_1614909258" CREATED="1677250985642" MODIFIED="1677251466744">
<icon BUILTIN="executable"/>
</node>
<node TEXT="returns the intersection of 2 tables" ID="ID_214559255" CREATED="1677251482373" MODIFIED="1677251490685"/>
</node>
<node TEXT="all the node that have at least one of two labels (OR)" ID="ID_1115839845" CREATED="1677251209181" MODIFIED="1677251235976">
<node TEXT="MATCH (m:Aldose|:Hexose)&#xa;RETURN m" ID="ID_815779744" CREATED="1677251350397" MODIFIED="1677251461816">
<icon BUILTIN="executable"/>
</node>
<node TEXT="returns the union of 2 tables" ID="ID_66696260" CREATED="1677251494053" MODIFIED="1677251504264"/>
</node>
</node>
</node>
<node TEXT="property" ID="ID_934936388" CREATED="1677251633582" MODIFIED="1677251641065">
<node TEXT="(key, value) pair" ID="ID_543231723" CREATED="1677251663262" MODIFIED="1677251839721"/>
<node TEXT="similar to the attributes of a table" ID="ID_89935111" CREATED="1677251749838" MODIFIED="1677251762617">
<node TEXT="key = attribute name" ID="ID_962293950" CREATED="1677251995119" MODIFIED="1677252001017"/>
<node TEXT="value = cell" ID="ID_1853672951" CREATED="1677252001422" MODIFIED="1677252005162"/>
</node>
<node TEXT="a node can have 0 or many properties" ID="ID_1651241508" CREATED="1677251772838" MODIFIED="1677251805288">
<node TEXT="no order between properties" ID="ID_1794725810" CREATED="1677251809597" MODIFIED="1677251830177"/>
<node TEXT="a node cannot have 2 properties with the same key" ID="ID_1371005201" CREATED="1677251865022" MODIFIED="1677251897113">
<node TEXT="but the value can be a JSON object" ID="ID_78085983" CREATED="1677251935478" MODIFIED="1677251950986"/>
</node>
</node>
<node TEXT="the nodes with a label do not necessarily all have the same property keys" ID="ID_454993859" CREATED="1677252054990" MODIFIED="1677252088937"/>
<node TEXT="syntax" ID="ID_228607808" CREATED="1677251679270" MODIFIED="1677251700762">
<node TEXT="{key1:value1, key2:value2,...}" ID="ID_552226940" CREATED="1677251701734" MODIFIED="1677251729912">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="query" ID="ID_992539981" CREATED="1677252118503" MODIFIED="1677252317954">
<node TEXT="creation" ID="ID_1882774011" CREATED="1677252335087" MODIFIED="1677252338187">
<node TEXT="CREATE ( {name:&quot;riri&quot;, birthdate:&quot;2015&quot;})" ID="ID_1072592625" CREATED="1677252339127" MODIFIED="1677252565494">
<icon BUILTIN="executable"/>
</node>
<node TEXT="NB: possible to create multiple nodes" ID="ID_1511485688" CREATED="1677252462719" MODIFIED="1677252530505">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="with the same properties (duplicates?)" ID="ID_704134554" CREATED="1677252490055" MODIFIED="1677252521548"/>
<node TEXT="with different properties (inconsistencies?)" ID="ID_1536732891" CREATED="1677252500056" MODIFIED="1677252517611"/>
<node TEXT="add constraints to the database" ID="ID_1610187936" CREATED="1677252535520" MODIFIED="1677252552435">
<icon BUILTIN="idea"/>
</node>
</node>
</node>
<node TEXT="retrieval" ID="ID_1339025336" CREATED="1677252581888" MODIFIED="1677252600458">
<node TEXT="MATCH ( {name:&quot;Alice&quot;})" ID="ID_801417139" CREATED="1677252601888" MODIFIED="1677254801720">
<icon BUILTIN="executable"/>
</node>
<node TEXT="use nodeName.key to retrieve the value" ID="ID_1923934162" CREATED="1677254967548" MODIFIED="1677255061945">
<node TEXT="MATCH (n)&#xa;WHERE n.name=&quot;Alice&quot;" ID="ID_1585261294" CREATED="1677254771275" MODIFIED="1677254799495">
<icon BUILTIN="executable"/>
<node TEXT="equivalent to MATCH ( {name:&quot;Alice&quot;})" ID="ID_1341442078" CREATED="1677255039494" MODIFIED="1677255045626"/>
</node>
<node TEXT="MATCH (n)&#xa;WHERE n.birthdate &gt; date(&quot;1972&quot;)" ID="ID_19495825" CREATED="1677254853549" MODIFIED="1677254892504">
<icon BUILTIN="executable"/>
</node>
</node>
</node>
<node TEXT="modification" ID="ID_903249372" CREATED="1677486493430" MODIFIED="1677486556917">
<node TEXT="MATCH (n {name:&quot;Alice&quot;})&#xa;SET n.birthdate=&quot;1995&quot;" ID="ID_1107569070" CREATED="1677486559045" MODIFIED="1677491407783">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="deletion" ID="ID_1427455766" CREATED="1677630417826" MODIFIED="1677630421183">
<node TEXT="DELETE ()" ID="ID_189893043" CREATED="1677630472538" MODIFIED="1677630509317">
<icon BUILTIN="executable"/>
<node TEXT="&quot;()&quot; represents a node, not a function call" ID="ID_476682312" CREATED="1677630510801" MODIFIED="1677630530991"/>
<node TEXT="requires that the node has no relationship (cf. DETACH DELETE)" ID="ID_222069124" CREATED="1677630552760" MODIFIED="1677630583748">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="&quot;DELETE ()&quot; matches all the nodes and delete them" ID="ID_110224048" CREATED="1677630592178" MODIFIED="1677630625687">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node TEXT="DETACH DELETE ()" ID="ID_1071840291" CREATED="1677630632962" MODIFIED="1677630862446">
<icon BUILTIN="executable"/>
<node TEXT="deletes the node + all its relationships" ID="ID_1786359163" CREATED="1677630681058" MODIFIED="1677630698535"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="relationships" POSITION="right" ID="ID_1883195467" CREATED="1677255096206" MODIFIED="1677255250810">
<edge COLOR="#0000ff"/>
<node TEXT="syntax" ID="ID_1690586340" CREATED="1677255112742" MODIFIED="1677255118142">
<node TEXT="--&gt;" ID="ID_398757915" CREATED="1677255119430" MODIFIED="1677255129154">
<icon BUILTIN="executable"/>
</node>
<node TEXT="direction" ID="ID_185349920" CREATED="1677629350303" MODIFIED="1677629360341">
<node TEXT="always oriented at creation time" ID="ID_1780207000" CREATED="1677629361559" MODIFIED="1677629371643"/>
<node TEXT="direction can be ignored when querying" ID="ID_1577611111" CREATED="1677629372015" MODIFIED="1677629439411"/>
</node>
</node>
<node TEXT="creation" ID="ID_417303192" CREATED="1677255201550" MODIFIED="1677255204634">
<node TEXT="create the nodes and the relationship between them" ID="ID_622252185" CREATED="1677255213236" MODIFIED="1677255227641">
<node TEXT="CREATE ( {name:&quot;Alice&quot;}) --&gt; ( {name:&quot;Bob&quot;})" ID="ID_1377302230" CREATED="1677255263958" MODIFIED="1677255486211">
<icon BUILTIN="executable"/>
</node>
<node TEXT="risk of duplicate creation" ID="ID_1304824781" CREATED="1677255317470" MODIFIED="1677255353425">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="retrieve existing nodes and create a relationship between them" ID="ID_346539768" CREATED="1677255228543" MODIFIED="1677255243345">
<node TEXT="MATCH (a:{name:&quot;Alice&quot;}), (b:{name:&quot;Bob&quot;})&#xa;CREATE (a) --&gt; (b)" ID="ID_177286474" CREATED="1677255366086" MODIFIED="1677255456187">
<icon BUILTIN="executable"/>
</node>
<node TEXT="requires that both nodes exist" ID="ID_1258628789" CREATED="1677255435127" MODIFIED="1677255450722">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node TEXT="identifier" ID="ID_1244599475" CREATED="1677255734791" MODIFIED="1677255743875">
<node TEXT="assigned at creation" ID="ID_1766836456" CREATED="1677255757518" MODIFIED="1677255763299"/>
<node TEXT="accessible" ID="ID_1333277557" CREATED="1677255764663" MODIFIED="1677627844289">
<node TEXT="via function id()" ID="ID_274176769" CREATED="1677627846556" MODIFIED="1677627852773">
<font STRIKETHROUGH="true"/>
</node>
<node TEXT="by function elementId()" ID="ID_1810039356" CREATED="1677627645971" MODIFIED="1677627772021">
<node TEXT="returns a node or relationship identifier" ID="ID_1688411280" CREATED="1677627772026" MODIFIED="1677627789912"/>
<node TEXT="unique among both nodes and relationships across all databases" ID="ID_706978570" CREATED="1677627790476" MODIFIED="1677627801924"/>
</node>
</node>
<node TEXT="there can be multiple (equivalent) relationships between 2 nodes" ID="ID_1043366447" CREATED="1677255657159" MODIFIED="1677255683178"/>
</node>
<node TEXT="label(s)" ID="ID_264439522" CREATED="1677256003144" MODIFIED="1677256006798">
<node TEXT="syntax" ID="ID_1792870078" CREATED="1677628172004" MODIFIED="1677628180367">
<node TEXT=":TYPE_NAME" ID="ID_1927634198" CREATED="1677628191197" MODIFIED="1677628204562">
<icon BUILTIN="executable"/>
</node>
<node TEXT="recommendation" ID="ID_1041770203" CREATED="1677628205132" MODIFIED="1677628222334">
<node TEXT="upper-case" ID="ID_218169265" CREATED="1677628222339" MODIFIED="1677628226176"/>
<node TEXT="underscore to separate words" ID="ID_918492394" CREATED="1677628226517" MODIFIED="1678188218209"/>
</node>
</node>
<node TEXT="a relationhip can have at most 1 type" ID="ID_1260646287" CREATED="1677256023080" MODIFIED="1677629311457">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="property" ID="ID_389591428" CREATED="1677256007400" MODIFIED="1677256012067">
<node TEXT="cf for nodes" ID="ID_988805222" CREATED="1677629596216" MODIFIED="1677629612189"/>
</node>
<node TEXT="similar to join" ID="ID_1398506999" CREATED="1677256340991" MODIFIED="1677256349359">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="queries" POSITION="right" ID="ID_974042023" CREATED="1676889067195" MODIFIED="1676889070007">
<edge COLOR="#00ff00"/>
<node TEXT="nodes" ID="ID_747395151" CREATED="1676889209067" MODIFIED="1676889210842">
<node TEXT="all the nodes" ID="ID_385821056" CREATED="1676889071811" MODIFIED="1676889074470"/>
<node TEXT="projection: all the nodes and a subset of their properties" ID="ID_1962675758" CREATED="1676889074907" MODIFIED="1676889130658"/>
<node TEXT="selection: all the nodes that match a condition" ID="ID_1108257421" CREATED="1676889158955" MODIFIED="1676889181667"/>
</node>
<node TEXT="relationships" ID="ID_67153258" CREATED="1676889221931" MODIFIED="1676933610199">
<node TEXT="between any nodes" ID="ID_1196462920" CREATED="1676997656884" MODIFIED="1676997662616"/>
<node TEXT="with conditions on the node(s)" ID="ID_895962391" CREATED="1676997663412" MODIFIED="1676997689224"/>
<node TEXT="with conditions on the relationship" ID="ID_1660601497" CREATED="1676997672108" MODIFIED="1676997684888"/>
</node>
<node TEXT="paths" ID="ID_318107474" CREATED="1676889225826" MODIFIED="1676997711451">
<icon BUILTIN="bookmark"/>
<node TEXT="patterns" ID="ID_1016844944" CREATED="1678188273526" MODIFIED="1678188276675">
<node TEXT="chaining" ID="ID_1065966243" CREATED="1678188348069" MODIFIED="1678188351860">
<node TEXT="(:nodeA) -[:rel1]-&gt; (:nodeB) -[:rel2]-&gt; (:nodeC)" ID="ID_796742500" CREATED="1678188290028" MODIFIED="1678188339356">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="diverging" ID="ID_759850502" CREATED="1678188352252" MODIFIED="1678188356587">
<node TEXT="(:nodeB) &lt;-[:rel1]- (:nodeA) -[:rel2]-&gt; (:nodeB)" ID="ID_1070712558" CREATED="1678188366174" MODIFIED="1678188409142">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="converging" ID="ID_100250880" CREATED="1678188357421" MODIFIED="1678188359996">
<node TEXT="(:nodeB) -[:rel1]-&gt; (:nodeA) &lt;-[:rel2]- (:nodeC)" ID="ID_425681102" CREATED="1678188420397" MODIFIED="1678188464446">
<icon BUILTIN="executable"/>
</node>
</node>
</node>
<node TEXT="variable length" ID="ID_350184913" CREATED="1678188277326" MODIFIED="1678188706222">
<node TEXT="-[relationName:RELATION_TYPE*minOcc..maxOcc]-&gt;" ID="ID_1499890735" CREATED="1678188527285" MODIFIED="1678188605400">
<icon BUILTIN="executable"/>
</node>
<node TEXT="if minOcc is ommitted, 1 is assumed" ID="ID_1243175514" CREATED="1678188633806" MODIFIED="1678188653447">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="if maxOcc is ommitted, no upper bound" ID="ID_1581446012" CREATED="1678188666637" MODIFIED="1678188677164"/>
</node>
</node>
<node TEXT="Cypher" ID="ID_234252716" CREATED="1677000291925" MODIFIED="1677000295233">
<node TEXT="MATCH -&gt; entities and topological constraints" ID="ID_891366695" CREATED="1677000296597" MODIFIED="1677000312327"/>
<node TEXT="WHERE -&gt; additional selection constraints" ID="ID_1086332877" CREATED="1677000312916" MODIFIED="1677000336492"/>
<node TEXT="RETURN" ID="ID_1267267388" CREATED="1677000336915" MODIFIED="1677000346682">
<node TEXT="projection" ID="ID_1281268190" CREATED="1677000347964" MODIFIED="1677000367753"/>
<node TEXT="can be composed of" ID="ID_1279110956" CREATED="1677000368028" MODIFIED="1677000373601">
<node TEXT="nodes" ID="ID_312462859" CREATED="1677000374868" MODIFIED="1677000376640"/>
<node TEXT="property values" ID="ID_1360554017" CREATED="1677000376908" MODIFIED="1677000388297"/>
<node TEXT="relationships" ID="ID_1010006369" CREATED="1677000388596" MODIFIED="1677000400020"/>
<node TEXT="paths" ID="ID_1639394793" CREATED="1677000400866" MODIFIED="1677000404448"/>
</node>
</node>
</node>
</node>
</node>
</map>
