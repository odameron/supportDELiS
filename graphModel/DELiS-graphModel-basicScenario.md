---
title: "Data Engineering in Life Sciences : Graph databases - basic scenario"
tags: teaching, database, graph, network, neo4j
version: 1.0
date: 2023-11-22
---

# Start neo4j

- `${NEO4J_HOME}/bin/neo4j start`
- open [http://localhost:7474](http://localhost:7474)
- history: CTRL-UP or CTRL-DOWN
- execution CTRL-ENTER


# Create some nodes

```cypher
CREATE (:Person {name:"Riri", birthdate:1974})
CREATE (:Person {name:"Fifi", birthdate:1972})
CREATE (:Person {name:"Loulou", birthdate:1981})
CREATE (:Person {name:"Picsou", birthdate:1946})
```

```cypher
MATCH (n)
RETURN n
```

Add labels to some of the nodes:
```cypher
MATCH (p:Person {name:"Picsou"})
SET p:Rich
RETURN p
```

```cypher
MATCH (p:Person)
WHERE p.name IN ["Riri", "Fifi", "Loulou"]
SET p:CastorJr
RETURN p
```


# Create some relationships

```cypher
MATCH (p:Person {name:"Picsou"}), (c:CastorJr)
CREATE (p) -[:isUncleOf]-> (c)
```

```cypher
MATCH (c1:CastorJr), (c2:CastorJr)
WHERE c1 <> c2
RETURN c1.name, c2.name
```

```cypher
MATCH (c1:CastorJr), (c2:CastorJr)
WHERE c1 <> c2
CREATE (c1) -[:isSiblingOf]-> (c2)
```

# Select some nodes

```cypher
MATCH (u:Person) -[:isUncleOf]-> (c:CastorJr) -[:isSiblingOf]-> (y:CastorJr)
WHERE y.birthdate < 1973 AND c.birthdate < 1980
RETURN u.name, c.name, y.name
```


# Select a subgraph

```cypher
MATCH (u:Person) -[:isUncleOf]-> (c:CastorJr) -[:isSiblingOf]-> (y:CastorJr)
WHERE y.birthdate < 1973 AND c.birthdate < 1980
RETURN (u), (c), (y)
```

# Select a path

```cypher
MATCH path = (u:Person) -[:isUncleOf]-> (c:CastorJr) -[:isSiblingOf]-> (y:CastorJr)
WHERE y.birthdate < 1973
RETURN path
```

```cypher
MATCH path = (u:Person) -[:isUncleOf]-> (c:CastorJr) -[:isSiblingOf]-> (y:CastorJr)
WHERE y.birthdate < 1973
RETURN nodes(path)
```

```cypher
MATCH path = (u:Person) -[:isUncleOf]-> (c:CastorJr) -[:isSiblingOf]-> (y:CastorJr)
WHERE y.birthdate < 1973
RETURN relationships(path)
```
