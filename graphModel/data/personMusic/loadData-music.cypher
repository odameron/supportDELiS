
//MATCH (n)
//DETACH DELETE (n)

CREATE CONSTRAINT personNameConstraint FOR (pers:Person) REQUIRE pers.name IS UNIQUE;
CREATE CONSTRAINT instrumentNameConstraint FOR (inst:Instrument) REQUIRE inst.name IS UNIQUE;


LOAD CSV WITH HEADERS FROM "file:///Person-sex-yearOfBirth.tsv" AS line FIELDTERMINATOR '\t'
CREATE (:Person {name: line.Person, gender:line.gender, birthdate:date(line.birthdate)});

LOAD CSV WITH HEADERS FROM "file:///Instrument-type.tsv" AS line FIELDTERMINATOR '\t'
CREATE (:Instrument {name: line.Instrument});
//LOAD CSV WITH HEADERS FROM "file:///Instrument-type.tsv" AS line FIELDTERMINATOR '\t'
//MATCH (instr:Instrument {name: line.Instrument})
//SET instr :line.category;

LOAD CSV WITH HEADERS FROM "file:///Person-knows.tsv" AS csvLine FIELDTERMINATOR '\t'
MATCH (pers1:Person {name: csvLine.Person}), (pers2:Person {name: csvLine.knows})
CREATE (pers1)-[:KNOWS]->(pers2);

LOAD CSV WITH HEADERS FROM "file:///Person-instrument.tsv" AS csvLine FIELDTERMINATOR '\t'
MATCH (pers:Person {name: csvLine.Person}), (inst:Instrument {name: csvLine.plays})
CREATE (pers)-[:PLAYS]->(inst);


