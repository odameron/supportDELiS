---
title: "Data Engineering in Life Sciences : Graph databases"
tags: teaching, database, graph, network, neo4j
version: 1.4
date: 2023-11-29
---


![Graph databases overview](graph_databases.png)


# Install neo4j

## Prefered method: do it yourself

NB: for Windows and macOS, see at the end of the paragraph.

- install java 17
	- check your java version with `java -version` (in a terminal)
	- if necessary:
```bash
# we probably will not need the jdk, so jre should be enough
sudo apt-get update
sudo apt-get openjdk-17-jre openjdk-17-jre-headless
#sudo apt-get install openjdk-17-jdk openjdk-17-jdk-headless openjdk-17-jre openjdk-17-jre-headless
```
- Download Neo4J **Community edition**
	- https://neo4j.com/download-center/
	- select "Community server" tab (right of the default "Enterprise server")
	- chose either a package for your distribution or the tarball (here we chose the tarball so that we know exactly what is installed where)
		- https://neo4j.com/docs/operations-manual/current/installation/linux/tarball/
```bash
cd /usr/local
sudo mv ~/Downloads/neo4j-community-5.5.0-unix.tar.gz .
sudo tar xvfz neo4j-community-5.5.0-unix.tar.gz && sudo rm neo4j-community-5.5.0-unix.tar.gz 
sudo ln -s neo4j-community-5.5.0/ neo4j
cd neo4j
sudo chown -R ${USER}:adm .

# for making your life easier you can declare a NEO4J_HOME environment variable
echo "export NEO4J_HOME=/usr/local/neo4j" >> ~/.bashrc
source ~/.bashrc
```

- start/stop neo4j : you can either
	- run neo4j in console mode from a terminal: 
		- run `${NEO4J_HOME}/bin/neo4j console`
		- do not close the terminal until you want to stop the server
		- run `CTRL-c` in the terminal to stop the server
	- run neo4j as a background process (you will get your terminal back after starting the server). From a terminal:
		- run `${NEO4J_HOME}/bin/neo4j start`
		- do whatever you want with your terminal
		- run `${NEO4J_HOME}/bin/neo4j stop` to stop the server
- in a browser, open http://localhost:7474
	- connection: `bolt://localhost:7687` (I had to change the protocol from `neo4j` to `bolt`)
	- authentication: username + password
	- user = `neo4j`
	- password = `neo4j` **(you will be required to change it)** 
- If you are on Windows and insist on using WSL (which we strongly discourage), well, you are on your own
	- maybe it will work right away (probably not)
	- try 
		- open a terminal in ubuntu
		- run `ifconfig`
		- find the `inet` IP address for `eth0`
		- open `conf/neo4j.conf` from your local installation of Neo4J with a text editor such as `nano`
			- look for the "Network connector configuration" section
			- uncomment the line `server.default_listen_address=...`
			- replace `0.0.0.0` by the IP address from step 3
		- open windows' powershell as administrator
			- run 'netsh interface portproxy add v4tov4 listenport=7474 listenaddress=0.0.0.0 connectport=7474 connectaddress=your-IP-address-from-step-3' (of course, replace ''your-IP-address-from-step-3")
			- same thing with port 7687 instead of 7474
	- if it doesn't, you can also install "Neo4J Desktop" in addition to the Neo4J server. Follow the [instructions here]([https://neo4j.com/docs/desktop-manual/current/visual-tour/](https://neo4j.com/docs/desktop-manual/current/visual-tour/)
	- (Tips courtesy of Corentin Dumortier and Axelle Dubois)
- If you are on macOS: the regular installations steps above are OK, but you need to install java-17 before (by H. Mingarelli)
	- Download Miniconda from https://docs.conda.io/projects/miniconda/en/latest/
	- Create a conda environment: `conda create -n java17envt`
	- Activate the conda environment : `conda activate java17envt`
	- Install the Java version 17 package in the environment: `conda install -c conda-forge openjdk=17`

## If installing by yourslef does not work: via docker

![Neo4J docker container life cycle](dockerContainerLifeCycle.png)

- the easiest way is to install the docker container
- NB: set of utility scripts for facilitating the interactions with Neo4J docker container: https://gitlab.com/odameron/neo4j-commands
    - `git clone https://gitlab.com/odameron/neo4j-commands.git`
    - `cd neo4j-commands`
    - `sudo install`
    - it provides the commands 
        - `neopull` for retrieving the docker container
        - `neostart` or `neostartpersistent` for starting the Neo4J server in ephemeral or persistent mode
        - `neostop` for stopping the Neo4J server
        - `neostatus` for checking the status of the server
- **download**
    - linux
        - (pre-requisite: install docker. **Warning:** this is the `docker.io` package)
        - from a terminal, retrieve the Neo4J container with either `neopull` or `docker pull neo4j`
    - Windows
        - ???
- **start** (choose 1 of 2 modes)
    - regular (ephemeral container: everything is lost when the container is closed) 
        - `neostart`
        - (user=neo4j; password=test) `docker run --name neo4jlatest -p7474:7474 -p7687:7687 -d --env NEO4J_AUTH=neo4j/test neo4j:latest`
    - persistent (your dataset is saved in the container before it is closed)
        - `neostartpersistent`
        - (user=neo4j; password=test) `docker run --name neo4jlatest -p7474:7474 -p7687:7687 -d -v $HOME/neo4j/data:/data -v $HOME/neo4j/logs:/logs -v $HOME/neo4j/import:/var/lib/neo4j/import -v $HOME/neo4j/plugins:/plugins --env NEO4J_AUTH neo4j/test neo4j:latest`
- **stop**
    - `neostop`
    - `docker rm -f neo4jlatest`






# Cypher Queries

For the moment, we will use the GUI at http://localhost:7474

See the section "Use Neo4j" for automatic access to the server.

![Cypher principles](Neo4J_principles.png)

Example dataset (see directory `data/personMusic/`)

![Music dataset](./data/personMusic/dataset-music.png)


## Nodes and labels

### create a node


```cypher
// anonymous node
CREATE ()
```

- observe the identifier
- create multiple persons (observe the identifier being automatically incremented)


```cypher
MATCH (n) RETURN (n)
```


- click on the graph view
    - hover on the node and observe its id
- click on the text view


```cypher
CREATE (:Person)
```


## Nodes and properties

```cypher
CREATE (:Person {name:"Riri"})
```


```cypher
MATCH (n:Person)
RETURN n.birthdate
ORDER BY n.birthdate
```

```cypher
MATCH (n:Person)
WHERE n.birthdate IS NOT NULL
RETURN n.birthdate
ORDER BY n.birthdate
```

```cypher
MATCH (n:Person {birthdate:date("1978")})
RETURN n
```

```cypher
// logically equivalent to the previous query
MATCH (n:Person)
WHERE n.birthdate=date("1978")
RETURN n
```

```cypher
MATCH (n:Person)
WHERE n.birthdate>date("1972")
RETURN n
```

## Relationships

```cypher
// who knows whom?
MATCH (p1) -[:KNOWS]-> (p2)
RETURN p1.name, p2.name
```

```cypher
// who does Alice know?
MATCH (p1:Person {name:"Alice"}) -[:KNOWS]-> (p2)
RETURN p2.name
```

```cypher
// who knows Alice?
// Notice that this is equivalent to
// MATCH (p2) -[:KNOWS]-> (p1:Person {name:"Alice"}) 
// but is easier to copy-paste from the previous query
MATCH (p1:Person {name:"Alice"}) <-[:KNOWS]- (p2)
RETURN p2.name
```

```cypher
// who is in Alice 1-neighborhood?
MATCH (p1:Person {name:"Alice"}) -[:KNOWS]- (p2)
RETURN p2.name
```

For all the previous queries, notice that:
- returning nodes instead of property values allows to display a graph
- you have to explicitely add the node `p1` to this graph if you want to

## Paths

```cypher
// who does Fred know (directly)?
MATCH (p1:Person {name:"Fred"}) -[:KNOWS]-> (p2)
RETURN p1,p2
```

```cypher
// who is known by someone that Fred knows?
// Notice that this is equivalent to
// MATCH (p1:Person {name:"Fred"}) -[:KNOWS]-> () -[:KNOWS]-> (p2)
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*2..2]-> (p2)
RETURN p1,p2
```

```cypher
// who is known by Fred or someone that Fred knows?
// by default the lower bound is 1
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*..2]-> (p2)
RETURN p1,p2
```

```cypher
// who is known by someone who is known by someone that Fred knows?
// by default the lower bound is 1
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*3..3]-> (p2)
RETURN p1,p2
```

```cypher
// idem with arbitrary bounds (e.g. 3 and 5)
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*3..5]-> (p2)
RETURN p1,p2
```

```cypher
// idem with no upper bounds
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*3..]-> (p2)
RETURN p1,p2
```

```cypher
// idem with transitive closure
MATCH (p1:Person {name:"Fred"}) -[:KNOWS*]-> (p2)
RETURN p1,p2
```

## Challenge

> **Objective:** identify the pairs of persons who do not know each other, who play the same instrument and such as one of them knows someone who knows the other

As a progression toward the solution, you can focus on the following steps; partial solutions are indicated below:
- the pairs of persons such as one knows the other
- the pairs of persons such as one does not know the other (is enough for the challenge)
- the pairs of persons such none of them knows the other (useless for the challenge, but still fun, there is a simple solution and a less-simple solution)
- the pairs of persons such as one knows someone who knows the other
- the pairs of persons who play the same instrument

```bash
# retrieve the pairs of persons (p1, p2) such as p1 and p2 play the same instrument, and p1 knows someone who knows p2
#  
# Observe that in the solution below there are some repetition (because p1 and p2 can be connected via different paths). 
# Use RETURN DISTINCT to avoid them
echo "TUFUQ0ggKGluc3RyOkluc3RydW1lbnQpIDwtWzpQTEFZU10tIChwMTpQZXJzb24pIC1bOktOT1dTXS0+ICg6UGVyc29uKSAtWzpLTk9XU10gLT4gKHAyOlBlcnNvbikgLVs6UExBWVNdLT4gKGluc3RyKQpSRVRVUk4gcDEubmFtZSwgcDIubmFtZSwgaW5zdHIubmFtZQo=" | base64 --decode
```

![pairs of persons (p1, p2) such as p1 and p2 play the same instrument, and p1 knows someone who knows p2](./figures/solution-music-withoutNegation.png)

```bash
# idem, but p1 does not know p2
echo "TUFUQ0ggKGluc3RyOkluc3RydW1lbnQpIDwtWzpQTEFZU10tIChwMTpQZXJzb24pIC1bOktOT1dTXS0+ICg6UGVyc29uKSAtWzpLTk9XU10gLT4gKHAyOlBlcnNvbikgLVs6UExBWVNdLT4gKGluc3RyKQpXSEVSRSBOT1QgRVhJU1RTIHsgKHAxKSAtWzpLTk9XU10tPiAocDIpIH0KLy9SRVRVUk4gcDEubmFtZSwgcDIubmFtZSwgaW5zdHIubmFtZQpSRVRVUk4gRElTVElOQ1QgcDEsIHAyLCBpbnN0cgo=" | base64 --decode
```

```bash
# idem, but p1 and p2 do not know each-other
echo "TUFUQ0ggKGluc3RyOkluc3RydW1lbnQpIDwtWzpQTEFZU10tIChwMTpQZXJzb24pIC1bOktOT1dTXS0+ICg6UGVyc29uKSAtWzpLTk9XU10gLT4gKHAyOlBlcnNvbikgLVs6UExBWVNdLT4gKGluc3RyKQpXSEVSRSBOT1QgRVhJU1RTIHsgKHAxKSAtWzpLTk9XU10tIChwMikgfQovL1JFVFVSTiBwMS5uYW1lLCBwMi5uYW1lLCBpbnN0ci5uYW1lClJFVFVSTiBESVNUSU5DVCBwMSwgcDIsIGluc3RyCg==" | base64 --decode
```



## Graph traversal

**TODO**
- shortest path
- path counting
- ...


## Load data

### Create nodes


```cypher
LOAD CSV WITH HEADERS FROM "file:///Person-sex-yearOfBirth.tsv" AS line FIELDTERMINATOR '\t'
CREATE (:Person {name: line.Person, gender:line.gender, birthdate:date(line.birthdate)})
```


### Create relationships

```cypher
LOAD CSV WITH HEADERS FROM "file:///Person-knows.tsv" AS line FIELDTERMINATOR '\t'

MATCH (p:Person {name:line.Person}), (f:Person {name:line.knows})

CREATE (p) -[:knows]-> (f)
```


# Use Neo4J

Once the Neo4J server is started, you can either:
- use the GUI
- use the command line
- use your favorite programming language



## GUI

- go to http://localhost:7474

## Command line

- install the `neo4j-client` package (documentation at https://neo4j-client.net/)
- make sure Neo4J is running (!)
- you can use either an interactive shell, or non-interactive calls
    - interactive shell
        - `neo4j-client -u neo4j localhost` will try to connect to `localhost:7687`
        - from there you should get a `neo4j>` prompt to enter your queries and see the result
    - non-interactive calls
        - either directly type your cypher queries in the command line (not very convenient), or save them in a file
        - `echo "MATCH (n:Person) RETURN n.name AS name, n.born AS born LIMIT 3" | neo4j-client -u neo4j -P localhost > result.csv`
        - `neo4j-client -u neo4j -P -o result.out -i query.cyp localhost`


## API

There is probably a Neo4J library for your favorite programming language (Python, Java, ...): https://neo4j.com/docs/getting-started/current/languages-guides/

The default Python library by Neo4J is... `neo4j`, ~~but the community-developped `py2neo` seems easier to use~~ **Update 2023-11-27:** `py2neo` has been retired and should be replaced with `neomodel` (https://medium.com/neo4j/py2neo-is-end-of-life-a-basic-migration-guide-9f11c10d76a3)

### neomodel Python library

**TODO:** update example

### py2neo Python library

```bash
pip3 install --user py2neo
```


```python
from py2neo import Graph

graph = Graph("bolt://localhost:7687", auth=("neo4j", "password"))

graph.run('CREATE (n:Person {name:"riri", age:17})')
graph.run('CREATE (n:Person {name:"fifi", age:18})')
graph.run('CREATE (n:Person {name:"loulou", age:19})')

graph.run('MATCH (p:Person) WHERE p.age<42 RETURN p')
# result:
#  p                                     
# ---------------------------------------
#  (_0:Person {age: 17, name: 'riri'})   
#  (_1:Person {age: 18, name: 'fifi'})   
#  (_2:Person {age: 19, name: 'loulou'}) 

graph.run('MATCH (p:Person) WHERE p.age<19 RETURN p')
# result:
#  p                                     
# ---------------------------------------
#  (_0:Person {age: 17, name: 'riri'})   
#  (_1:Person {age: 18, name: 'fifi'})   
```



### Default neo4j Python library

```bash
# FROM A TERMINAL
pip3 install --user neo4j
```

```python
# FROM PYTHON
from neo4j import GraphDatabase
with GraphDatabase.driver('neo4j://localhost', auth=AUTH) as driver:
    driver.verify_connectivity()


# CREATE NODES -> use execute_write(...)
def create_person_node(tx, name, age):
    result = tx.run("CREATE(n:Person {name:$pname, age:$page})", pname=name, page=age)
    records = list(result)
    summary = result.consume()
    return records, summary

with GraphDatabase.driver('neo4j://localhost', auth=AUTH) as driver:
    with driver.session(database="neo4j") as session:
        records, summary = session.execute_write(create_person_node, name="riri", age=19)
        records, summary = session.execute_write(create_person_node, name="fifi", age=18)
        records, summary = session.execute_write(create_person_node, name="loulou", age=18)


# QUERY NODES -> use execute_read(...)
def match_person_nodes(tx, age):
    result = tx.run("MATCH (p:Person) WHERE p.age < $page RETURN p", page=age)
    records = list(result)
    summary = result.consume()
    return records, summary

with GraphDatabase.driver('neo4j://localhost', auth=AUTH) as driver:
    with driver.session(database="neo4j") as session:
        records, summary = session.execute_read(match_person_nodes, age=19)
        for currentPerson in records:
            print(currentPerson)
```




# Resources

- Cypher: 
    - getting started: https://neo4j.com/docs/getting-started/current/cypher-intro/
    - Cypher manual: https://neo4j.com/docs/cypher-manual/current/
    - Cypher cheat sheet: https://neo4j.com/docs/cypher-cheat-sheet/current/
- https://nicolewhite.github.io/neo4j-jupyter/hello-world.html
