<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Graph databases" FOLDED="false" ID="ID_753416008" CREATED="1676888854519" MODIFIED="1676888863158" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="motivation" POSITION="right" ID="ID_1788298310" CREATED="1676889260459" MODIFIED="1676889264462">
<edge COLOR="#ff00ff"/>
<node TEXT="tabulated files / spreadsheet" ID="ID_592531129" CREATED="1676889268931" MODIFIED="1676889284057">
<node TEXT="describe 1 category of entities by file / spreadsheet" ID="ID_1953264175" CREATED="1676889291300" MODIFIED="1676933331854">
<icon BUILTIN="idea"/>
</node>
<node TEXT="OK: dependencies and relations btw entities of the same category" ID="ID_874282470" CREATED="1676889321083" MODIFIED="1676889545461">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="not well adapted to dependencies/relations with entities of other categories" ID="ID_1891607892" CREATED="1676889371539" MODIFIED="1676889538767">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="query expressivity limited to the basic operations" ID="ID_813511556" CREATED="1676889483715" MODIFIED="1676889549146">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="relational model" ID="ID_1952959007" CREATED="1676889402738" MODIFIED="1676889407814">
<node TEXT="focus: a node and its direct neighbors" ID="ID_970579448" CREATED="1676933245635" MODIFIED="1676933335230">
<icon BUILTIN="idea"/>
</node>
<node TEXT="entities organised by categories" ID="ID_1988366802" CREATED="1676889442395" MODIFIED="1676889452746"/>
<node TEXT="formal and expressive query language" ID="ID_1770204232" CREATED="1676889564348" MODIFIED="1676933534542">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="OK: direct relations between entities" ID="ID_1222141759" CREATED="1676932916867" MODIFIED="1676933216966">
<icon BUILTIN="button_ok"/>
<node TEXT="relations are named" ID="ID_1309138561" CREATED="1676933173340" MODIFIED="1676933205847"/>
<node TEXT="relations can have properties (requires reification)" ID="ID_65342484" CREATED="1676933185203" MODIFIED="1676933201615"/>
</node>
<node TEXT="you have to know exactly which relation to follow" ID="ID_235604687" CREATED="1676932958731" MODIFIED="1676933139139">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="relations are represented differently according to their cardinality" ID="ID_1946215295" CREATED="1676932981195" MODIFIED="1676933141040">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="relations traversal" ID="ID_271439972" CREATED="1676933041451" MODIFIED="1676933143564">
<icon BUILTIN="messagebox_warning"/>
<node TEXT="has to be performed explicitely" ID="ID_1097974215" CREATED="1676933119411" MODIFIED="1676933126062"/>
<node TEXT="quickly becomes painful" ID="ID_1938660755" CREATED="1676933126420" MODIFIED="1676933135303"/>
</node>
</node>
<node TEXT="graph model" ID="ID_445127348" CREATED="1676933262907" MODIFIED="1676933266655">
<node TEXT="focus: general organisation of the entities" ID="ID_1530535382" CREATED="1676933269195" MODIFIED="1676933328167">
<icon BUILTIN="idea"/>
</node>
<node TEXT="expressive query language" ID="ID_188434368" CREATED="1676933476588" MODIFIED="1676933528263">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="graph traversal queries" ID="ID_626022320" CREATED="1676933497581" MODIFIED="1676933510271">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="labels (entities&apos; categories)" ID="ID_196912031" CREATED="1676933380316" MODIFIED="1676933390751">
<node TEXT="can be explicit" ID="ID_144622156" CREATED="1676933392564" MODIFIED="1676933436541">
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="are a different kind of nodes" ID="ID_355947716" CREATED="1676933409508" MODIFIED="1676933440226">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="difficult/impossible to represent relations between labels" ID="ID_1055184832" CREATED="1676933416635" MODIFIED="1676933442721">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
</node>
<node TEXT="nodes" POSITION="right" ID="ID_1851292932" CREATED="1676888902355" MODIFIED="1676888955504">
<edge COLOR="#ff0000"/>
<node TEXT="identifier" ID="ID_746976942" CREATED="1676888956866" MODIFIED="1676888959158">
<node TEXT="generated automatically" ID="ID_628923792" CREATED="1676996721427" MODIFIED="1676996726871"/>
<node TEXT="you should not interfere with it" ID="ID_92031297" CREATED="1676996727218" MODIFIED="1676996735944"/>
</node>
<node TEXT="properties" ID="ID_1349557035" CREATED="1676888959522" MODIFIED="1676888966606">
<node TEXT="similar to SQL tables&apos; attributes" ID="ID_531645632" CREATED="1676889134826" MODIFIED="1676997245930">
<icon BUILTIN="info"/>
</node>
<node TEXT="each property has" ID="ID_1987479858" CREATED="1676996766786" MODIFIED="1676996787589">
<node TEXT="a name (unique for the node)" ID="ID_1151425174" CREATED="1676996788946" MODIFIED="1676996797858"/>
<node TEXT="a value" ID="ID_830597064" CREATED="1676996798192" MODIFIED="1676996807575"/>
</node>
<node TEXT="a node can have 0, 1 or many properties" ID="ID_1070389759" CREATED="1676996809554" MODIFIED="1676996858978"/>
</node>
<node TEXT="labels" ID="ID_1724223400" CREATED="1676888967097" MODIFIED="1676932837042">
<node TEXT="aka categories, classes,..." ID="ID_247633642" CREATED="1676932838075" MODIFIED="1676932843213"/>
<node TEXT="similar to SQL tables or categories" ID="ID_283738801" CREATED="1676996865194" MODIFIED="1676997260978">
<icon BUILTIN="info"/>
</node>
<node TEXT="a node can have 0, 1 or many labels" ID="ID_1823895444" CREATED="1676932844147" MODIFIED="1676932859236"/>
</node>
</node>
<node TEXT="relationships" POSITION="right" ID="ID_1921241149" CREATED="1676888984554" MODIFIED="1676933577083">
<edge COLOR="#0000ff"/>
<node TEXT="between 2 nodes" ID="ID_1887662858" CREATED="1676997126691" MODIFIED="1676997131712"/>
<node TEXT="directed (but direction can be ignored in queries)" ID="ID_349050830" CREATED="1676997132283" MODIFIED="1676997151519"/>
<node TEXT="each relationship has its own identifier" ID="ID_963135362" CREATED="1676997447099" MODIFIED="1676997469021">
<node TEXT="generated automatically" ID="ID_1658584083" CREATED="1676997498915" MODIFIED="1676997509943"/>
<node TEXT="you should not interfere with it" ID="ID_600241435" CREATED="1676997479579" MODIFIED="1676997495535"/>
<node TEXT="consequence: there can be multiple relationships between 2 nodes" ID="ID_1399821472" CREATED="1676997309675" MODIFIED="1676997551952"/>
</node>
<node TEXT="replace SQL join" ID="ID_1002710441" CREATED="1676997219785" MODIFIED="1676997239807">
<icon BUILTIN="info"/>
</node>
<node TEXT="can also have properties" ID="ID_682056560" CREATED="1676889000435" MODIFIED="1676997114385">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="can also have labels" ID="ID_723503072" CREATED="1676889004811" MODIFIED="1676933595275">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="queries" POSITION="right" ID="ID_974042023" CREATED="1676889067195" MODIFIED="1676889070007">
<edge COLOR="#00ff00"/>
<node TEXT="nodes" ID="ID_747395151" CREATED="1676889209067" MODIFIED="1676889210842">
<node TEXT="all the nodes" ID="ID_385821056" CREATED="1676889071811" MODIFIED="1676889074470"/>
<node TEXT="projection: all the nodes and a subset of their properties" ID="ID_1962675758" CREATED="1676889074907" MODIFIED="1676889130658"/>
<node TEXT="selection: all the nodes that match a condition" ID="ID_1108257421" CREATED="1676889158955" MODIFIED="1676889181667"/>
</node>
<node TEXT="relationships" ID="ID_67153258" CREATED="1676889221931" MODIFIED="1676933610199">
<node TEXT="between any nodes" ID="ID_1196462920" CREATED="1676997656884" MODIFIED="1676997662616"/>
<node TEXT="with conditions on the node(s)" ID="ID_895962391" CREATED="1676997663412" MODIFIED="1676997689224"/>
<node TEXT="with conditions on the relationship" ID="ID_1660601497" CREATED="1676997672108" MODIFIED="1676997684888"/>
</node>
<node TEXT="paths" ID="ID_318107474" CREATED="1676889225826" MODIFIED="1676997711451">
<icon BUILTIN="bookmark"/>
</node>
<node TEXT="Cypher" ID="ID_234252716" CREATED="1677000291925" MODIFIED="1677000295233">
<node TEXT="MATCH -&gt; entities and topological constraints" ID="ID_891366695" CREATED="1677000296597" MODIFIED="1677000312327"/>
<node TEXT="WHERE -&gt; additional selection constraints" ID="ID_1086332877" CREATED="1677000312916" MODIFIED="1677000336492"/>
<node TEXT="RETURN" ID="ID_1267267388" CREATED="1677000336915" MODIFIED="1677000346682">
<node TEXT="projection" ID="ID_1281268190" CREATED="1677000347964" MODIFIED="1677000367753"/>
<node TEXT="can be composed of" ID="ID_1279110956" CREATED="1677000368028" MODIFIED="1677000373601">
<node TEXT="nodes" ID="ID_312462859" CREATED="1677000374868" MODIFIED="1677000376640"/>
<node TEXT="property values" ID="ID_1360554017" CREATED="1677000376908" MODIFIED="1677000388297"/>
<node TEXT="relationships" ID="ID_1010006369" CREATED="1677000388596" MODIFIED="1677000400020"/>
<node TEXT="paths" ID="ID_1639394793" CREATED="1677000400866" MODIFIED="1677000404448"/>
</node>
</node>
</node>
</node>
</node>
</map>
