# Python

```bash
pip3 install --user py2neo
```


```python
from py2neo import Graph

graph = Graph("neo4j://localhost:7687")

graph.run('CREATE(n:Person {name:"riri", age:17})')
graph.run('CREATE(n:Person {name:"fifi", age:18})')
graph.run('CREATE(n:Person {name:"loulou", age:19})')

graph.run('MATCH (p:Person) WHERE p.age<42 RETURN p')
# result:
#  p                                     
# ---------------------------------------
#  (_0:Person {age: 17, name: 'riri'})   
#  (_1:Person {age: 18, name: 'fifi'})   
#  (_2:Person {age: 19, name: 'loulou'}) 

graph.run('MATCH (p:Person) WHERE p.age<19 RETURN p')
# result:
#  p                                     
# ---------------------------------------
#  (_0:Person {age: 17, name: 'riri'})   
#  (_1:Person {age: 18, name: 'fifi'})   
```


# Cypher queries




## Nodes + properties and labels

### create a node

> Nodes are delimited by parentheses
> `()` represents a node with no additional contraint
> `(n)` represents a node that we call `n`; note that `n` is a name that *we* give to the node (e.g. to reuse it later in the query). Its scope is the query, where it behaves like a variable name, but it does not belong to the database.



```cypher
CREATE ()
```

```cypher
# here we give a name to the node. 
# The name will not be stored in the database...
# ... and is useless here because we do not reuse it
CREATE (n)
```

```cypher
MATCH (n) RETURN (n)
```


- click on the graph view
    - hover on the node and observe its id
- click on the text view



### Nodes and properties

> Nodes can have 0, 1 or many properties.
> Each property has a name (unique for the node) and an associated value.
> The properties of a node can been seen as a Python dictionary.
> Notation: `( {propertyName1:propertyValue1, propertyName2:propertyValue2, ...)`


```cypher
CREATE ( {name:"Riri", age:19})
```

```cypher
MATCH (n) RETURN (n)
```

Now, we can retrieve only some of the properties (aka projection)

```cypher
# note that since we retrieve some property we do not need the parentheses
MATCH (n) RETURN n.name
```

```cypher
MATCH (n) RETURN n.name, n.age
```


Now, we can apply condition(s) on the properties (aka selection)

```cypher
# exact match (version 1)
MATCH (n {name:"Fifi"}) RETURN (n)
```

```cypher
# exact match (version 2)
MATCH (n) WHERE n.name="Fifi" RETURN (n)
```

```cypher
# complex conditions
MATCH (n) WHERE n.age < 42 RETURN (n)
```

```cypher
# multiple complex conditions
MATCH (n) WHERE n.age < 42 AND n.name="Loulou" RETURN (n)
```

We can add some new property to an existing node

```cypher
MATCH (n {name:"Fifi"}) SET n.location="Toonville" RETURN (n)
```

We can change the property value of an existing node

```cypher
MATCH (n {name:"Fifi"}) SET n.location="Rennes" RETURN (n)
```


### Nodes and labels


```cypher
# create an empty node with the label Person
CREATE ( :Person)
```

```cypher
# retrieve all the nodes with the label Person
MATCH (n:Person) RETURN (n)
```

```cypher
# create a node with some properties and a label
CREATE (:Person {name:"Picsou", age:53})
```

```cypher
# retrieve the label(s) of a node
MATCH (n {name:"Picsou"}) RETURN labels(n)
```

```cypher
# create a node with multiple labels
CREATE (:Person:CastorJr {name:"Riri", age:17})
```





## Relations

- create a relation
    - there can be several instances of a relation between two nodes
    - relations can have properties
    - relations can have a type
- retrieve relations
    - according to their label
    - from their starting node
    - from their ending node
    - between two nodes
    - retrieve the labels of a relation between two nodes


## Paths

## Graph traversal


# 2023-2024

- negation
- `ALL` and `ANY`
- aggregation



# Resources

- https://nicolewhite.github.io/neo4j-jupyter/hello-world.html
