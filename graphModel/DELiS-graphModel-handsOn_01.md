---
title: Data Engineering in Life Sciences : Graph databases
tags: database, graph, network, neo4j
version: 1.4
date: 2023-12-02
---

# Context

A (fictional) gene expression analysis yields a cluster of proteins statistically correlated to a phenotype of interest.

**The goal is to infer relations between some of these proteins  using biological knowledge.**
We will consider:
- the interactions between proteins (the relation is symmetric, so `prot1 interactsWith prot2` and `prot2 interactsWith prot1` are both represented explicitely. It is not transitive)
- the regulation of biochemical reactions by proteins
- the production or consumption of proteins by biochemical reactions
- the chaining of biochemical reactions (when a molecule produced by a reaction is consumed by another reaction; for the sake of simplicity, we will not take the molecules into account but represent the relation directly between the reactions).  

> We want to identify the relations from `protein1` to `protein2`  such as `protein1` can influence a biochemical reaction (directly if the protein regulates the reaction, or indirectly if it interacts with a protein that regulates the reaction) that is upstream from another reaction that consumes or produces `protein2`. 

Note that this is typically a graph traversal problem, and that it would be difficult to address it using the relational model.

# Dataset

Start your Neo4J server and go to http://localhost:7474

The following command allows you to delete all the nodes and the relationships:
```cypher
MATCH (n) DETACH DELETE (n)
```


> **Question:** Create the following data using the labels `Protein` and `Reaction`, the property `name`, and the relationship types `interactsWith`, `regulates`, `consumes`, `produces`, and `hasNext`. (tip: save the instructions in a script such as `handsOn-dataset-load.cypher` so that you can easily tweak it)

![Dataset](figures/proteins-reactions-legend.png)




# Cypher Queries

NB: in all the solutions below, the attributes values should be quoted (e.g. `(:Protein {name:"Prot5"})` and not ~~`(:Protein {name:Prot5})`~~


> **Question:** Retrieve all the nodes

```bash
# SOLUTION:
echo "TUFUQ0ggKG4pClJFVFVSTiBu" | base64 --decode
```


> **Question:** How many nodes are there?

```bash
# SOLUTION:
echo 'TUFUQ0ggKG4pIFJFVFVSTiBjb3VudChuKQo=' | base64 --decode
```

## Labels


> **Question:** Retrieve all the proteins

> RETURN n"
```bash
# SOLUTION:
echo "TUFUQ0ggKG46UHJvdGVpbikKUkVUVVJOIG4=" | base64 --decode
```


> **Question:** How many nodes are proteins?

```bash
# SOLUTION:
echo "TUFUQ0ggKG46UHJvdGVpbikKUkVUVVJOIGNvdW50KERJU1RJTkNUIG4p" | base64 --decode
```


## Properties


> **Question:** What are the values of the `name` properties of the nodes?

```bash
# SOLUTION:
echo "TUFUQ0ggKG4pClJFVFVSTiBuLm5hbWU=" | base64 --decode
```


> **Question:** What are the values of the `name` properties of the proteins?

```bash
# SOLUTION:
echo "TUFUQ0ggKG46UHJvdGVpbikKUkVUVVJOIG4ubmFtZQ==" | base64 --decode
```


## Relationships


> **Question:** What are the reactions just after `react2`?

```bash
# SOLUTION:
echo "TUFUQ0ggKDpSZWFjdGlvbiB7bmFtZToicmVhY3QyIn0pIC1bOmhhc05leHRdLT4gKHI6UmVhY3Rpb24pClJFVFVSTiBy" | base64 --decode
```


> **Question:** What are the reactions just before `react2`?

```bash
# SOLUTION:
echo "TUFUQ0ggKHI6UmVhY3Rpb24pIC1bOmhhc05leHRdLT4gKDpSZWFjdGlvbiB7bmFtZToicmVhY3QyIn0pClJFVFVSTiBy" | base64 --decode
```


## Paths

![Path patterns and their Cypher counterpart](./figures/cypher-path.png)


> **Question:** What are the reactions downstream of `react2`?

```bash
# SOLUTION:
echo "Ly8gdXNlIFs6aGFzTmV4dCowLi5dIHRvIGFsc28gaW5jbHVkZSByZWFjdDIKLy8gKGUuZy4gdG8gZGlzcGxheSB0aGUgbm9kZXMpCk1BVENIICg6UmVhY3Rpb24ge25hbWU6InJlYWN0MiJ9KSAtWzpoYXNOZXh0KjEuLl0tPiAocjpSZWFjdGlvbikKUkVUVVJOIHI=" | base64 --decode
```


> **Question:** What are the reactions that are regulated by a protein?

```bash
# SOLUTION:
echo "TUFUQ0ggKDpQcm90ZWluKSAtWzpyZWd1bGF0ZXNdLT4gKHI6UmVhY3Rpb24pCnJldHVybiBy" | base64 --decode
```


> **Question:** What are the reactions that are regulated by the protein `prot5` ?

```bash
# SOLUTION:
echo "TUFUQ0ggKDpQcm90ZWluIHtuYW1lOiJwcm90NSJ9KSAtWzpyZWd1bGF0ZXNdLT4gKHI6UmVhY3Rpb24pCnJldHVybiBy" | base64 --decode
```


> **Question:** What are the nodes that (directly) regulate a reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKHApIC1bOnJlZ3VsYXRlc10tPiAoOlJlYWN0aW9uKQpyZXR1cm4gcA==" | base64 --decode
```


> **Question:** What are the proteins that (directly) regulate `react6`?

```bash
# SOLUTION:
echo "TUFUQ0ggKHApIC1bOnJlZ3VsYXRlc10tPiAoOlJlYWN0aW9uIHtuYW1lOiJyZWFjdDYifSkKcmV0dXJuIHA=" | base64 --decode
```


> **Question:** What are the proteins that interact with a protein that regulates a reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKHApIC1bOmludGVyYWN0c1dpdGhdLT4gKCkgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pCnJldHVybiBw" | base64 --decode
```


> **Question:** What are the proteins that (indirectly) regulate `react6`?

```bash
# SOLUTION:
echo "TUFUQ0ggKHApIC1bOmludGVyYWN0c1dpdGhdLT4gKCkgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24ge25hbWU6InJlYWN0NiJ9KQpyZXR1cm4gcA==" | base64 --decode
```


> **Question:** What are the proteins that (directly or indirectly) regulate `react6`? 
> NB: here "directly or indirectly regulates..." means that the protein either regulates the reaction, or interacts with a protein that regulates the reaction.

```bash
# SOLUTION:
echo "TUFUQ0ggKHApIC1bOmludGVyYWN0c1dpdGgqMC4uMV0tPiAoKSAtWzpyZWd1bGF0ZXNdLT4gKDpSZWFjdGlvbiB7bmFtZToicmVhY3Q2In0pCnJldHVybiBw" | base64 --decode
```


> **Question:** What are the proteins that are consumed or produced by `react6`?

```bash
# SOLUTION:
echo "TUFUQ0ggKDpSZWFjdGlvbiB7bmFtZToicmVhY3Q2In0pIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAocDpQcm90ZWluKSAKUkVUVVJOIHA=" | base64 --decode
```


> **Question:** What are the proteins that are consumed or produced downstream of `react2`?

```bash
# SOLUTION:
echo "TUFUQ0ggKDpSZWFjdGlvbiB7bmFtZToicmVhY3QyIn0pIC1bOmhhc05leHQqMC4uXS0+ICgpIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAocDpQcm90ZWluKSAKUkVUVVJOIHA=" | base64 --decode
```


> **Question:** What are the pairs of proteins where the first element regulates a reaction, and the second is consumed or produced by this reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKUkVUVVJOIGNvbnRyb2xsZXIsY29udHJvbGxlZA==" | base64 --decode
```


> **Question:** What are the pairs of proteins where the first element directly or indirectly regulates a reaction, and the second is consumed or produced by this reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6aW50ZXJhY3RzV2l0aCowLi4xXS0+ICg6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKUkVUVVJOIGNvbnRyb2xsZXIsY29udHJvbGxlZA==" | base64 --decode
```


> **Question:** What are the pairs of proteins where the first element directly regulates a reaction, and the second is consumed or produced downstream of this reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmhhc05leHQqMC4uXS0+ICgpIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKUkVUVVJOIGNvbnRyb2xsZXIsY29udHJvbGxlZA==" | base64 --decode
```


> **Question:** What are the pairs of proteins where the first element directly or indirectly regulates a reaction, and the second is consumed or produced downstream of this reaction?

```bash
# SOLUTION:
echo "TUFUQ0ggKGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6aW50ZXJhY3RzV2l0aCowLi4xXS0+ICg6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmhhc05leHQqMC4uXS0+ICgpIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKUkVUVVJOIGNvbnRyb2xsZXIsY29udHJvbGxlZA==" | base64 --decode
```


> **Question:** Adapt the previous query by giving a name (e.g. `controlPath`) to the path between the elements of the pairs of proteins from the previous question (`MATCH controlPath = ...`) and return this path. Use the graph view of Neo4J to visualize it

```bash
# SOLUTION:
echo "TUFUQ0ggY29udHJvbFBhdGg9KGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmhhc05leHQqMC4uXS0+ICgpIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKUkVUVVJOIGNvbnRyb2xQYXRo" | base64 --decode
```


> **Question:** Write a query that generates the `mayHaveImpactOn` relationships between the elements of the pairs of proteins from the previous question

```bash
# SOLUTION:
echo "TUFUQ0ggKGNvbnRyb2xsZXI6UHJvdGVpbikgLVs6cmVndWxhdGVzXS0+ICg6UmVhY3Rpb24pIC1bOmhhc05leHQqMC4uXS0+ICgpIC1bOmNvbnN1bWVzT3JQcm9kdWNlc10tPiAoY29udHJvbGxlZDpQcm90ZWluKSAKQ1JFQVRFIChjb250cm9sbGVyKSAtWzptYXlIYXZlSW1wYWN0T25dLT4gKGNvbnRyb2xsZWQp" | base64 --decode
```


> **Question:** Write a query to retreive the `mayHaveImpactOn` relationships between proteins, and visualize the network

```bash
# SOLUTION:
echo "TUFUQ0ggY29udHJvbFBhdGggPSAoY29udHJvbGxlcjpQcm90ZWluKSAtWzptYXlIYXZlSW1wYWN0T25dLT4gKGNvbnRyb2xsZWQ6UHJvdGVpbikKUkVUVVJOIGNvbnRyb2xQYXRo" | base64 --decode
```

![Protein interaction network](figures/solution-proteinControl.png)


# Resources

- Cypher: 
    - getting started: https://neo4j.com/docs/getting-started/current/cypher-intro/
    - Cypher manual: https://neo4j.com/docs/cypher-manual/current/
    - Cypher cheat sheet: https://neo4j.com/docs/cypher-cheat-sheet/current/
- https://nicolewhite.github.io/neo4j-jupyter/hello-world.html
